Notebooks
=========

..  nbgallery::

    share/galactic/core/concept/notebooks/context
    share/galactic/core/concept/notebooks/connection
    share/galactic/core/concept/notebooks/concept
    share/galactic/core/concept/notebooks/lattice
    share/galactic/core/concept/notebooks/family
    share/galactic/core/concept/notebooks/algorithms

Notes [#Moore]_ [#Galois]_.

.. [#Moore]
        The photo of Eliakim Hastings Moore Moore has been taken
        from `https://en.wikipedia.org/wiki/E._H._Moore <https://en.wikipedia.org/wiki/E._H._Moore>`_
        and is in the public domain.

.. [#Galois]
        The drawing of Évariste Galois has been taken
        from `https://en.wikipedia.org/wiki/Évariste_Galois <https://en.wikipedia.org/wiki/%C3%89variste_Galois>`_
        and is in the public domain.
