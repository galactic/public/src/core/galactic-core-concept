Concepts
========

Classes and functions
---------------------

.. automodule:: galactic.algebras.concept
    :members:
    :inherited-members:

Renderers
---------

.. automodule:: galactic.algebras.concept.renderer
    :members:
    :inherited-members:
