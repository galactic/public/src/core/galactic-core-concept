from galactic.algebras.concept import create_context_from_data

animals = {
    "Dove": ["small", "twolegs", "feathers", "fly"],
    "Hen": ["small", "twolegs", "feathers"],
    "Duck": ["small", "twolegs", "feathers", "fly", "swim"],
    "Goose": ["small", "twolegs", "feathers", "fly", "swim"],
    "Owl": ["small", "twolegs", "feathers", "fly", "hunt"],
    "Hawk": ["small", "twolegs", "feathers", "fly", "hunt"],
    "Eagle": ["medium", "twolegs", "feathers", "fly", "hunt"],
    "Fox": ["medium", "fourlegs", "hunt", "hair", "run"],
    "Dog": ["medium", "fourlegs", "hair", "run"],
    "Wolf": ["medium", "fourlegs", "hair", "hunt", "run", "mane"],
    "Cat": ["small", "fourlegs", "hair", "hunt", "run"],
    "Tiger": ["big", "fourlegs", "hair", "hunt", "run"],
    "Lion": ["big", "fourlegs", "hair", "hunt", "run", "mane"],
    "Horse": ["big", "fourlegs", "hair", "run", "mane", "hooves"],
    "Zebra": ["big", "fourlegs", "hair", "run", "mane", "hooves"],
    "Cow": ["big", "fourlegs", "hair", "hooves"],
}

context = create_context_from_data(animals)
