"""
The :mod:`galactic.algebras.examples.concept` module.

It defines two variables:

* :data:`animals` a dictionary containing a sample table;
* :data:`context` a binary context representing the :data:`animals` dictionary.

Examples
--------
>>> from galactic.algebras.examples.concept import context
>>> context
<galactic.algebras.concept.Context object at 0x...>
>>> context.domain
<galactic.algebras.set.FiniteUniverse object at 0x...>
>>> list(context.domain)
['Dove', 'Hen', 'Duck', 'Goose', ..., 'Tiger', 'Lion', 'Horse', 'Zebra', 'Cow']
>>> context.co_domain
<galactic.algebras.set.FiniteUniverse object at 0x...>
>>> list(context.co_domain)
[<function small at 0x...>, <function twolegs at 0x...>, ...]
>>> list(context)
[('Dove', <function small at 0x...>), ..., ('Cow', <function hooves at 0x...>)]
"""

from ._animals import animals, context

__all__ = ("animals", "context")
