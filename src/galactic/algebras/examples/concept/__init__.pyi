from collections.abc import Sequence

from galactic.algebras.concept import Context

animals: dict[str, Sequence[str]]
context: Context
