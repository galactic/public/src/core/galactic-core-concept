"""Connectiopn module."""

from __future__ import annotations

import itertools
from typing import TYPE_CHECKING, cast

from typing_extensions import Self

from galactic.algebras.closure import (
    AbstractClosure,
    ClosedEnumerableMixin,
)
from galactic.algebras.connection import (
    GaloisConnectionMixin,
)
from galactic.algebras.lattice import Element
from galactic.algebras.set import FiniteSubSet
from galactic.helpers.core import default_repr

from ._attribute import Attribute
from ._context import Context

if TYPE_CHECKING:
    from collections.abc import Iterable, Iterator


# pylint: disable=too-many-ancestors
class Extent(ClosedEnumerableMixin[str], Element):  # type: ignore[misc]
    """
    It represents enumerable closed set of object identifiers.

    Parameters
    ----------
    connection
        The antitone Galois connection.
    elements
        The attributes shared by the elements in the closed set.

    """

    __slots__ = (
        "_hash",
        "_connection",
        "_elements",
    )

    def __init__(
        self,
        connection: GaloisConnection,
        elements: Iterable[Attribute] | None = None,
    ) -> None:
        self._hash: int | None = None
        self._connection = connection
        attributes = list(elements or [])
        self._elements = FiniteSubSet[str](
            connection.context.universes[0],
            (
                key
                for key in connection.context.universes[0]
                if all(
                    (key, attribute) in connection.context for attribute in attributes
                )
            ),
        )

    def __hash__(self) -> int:
        if self._hash is None:
            self._hash = hash((self._connection, self._elements))
        return self._hash

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __len__(self) -> int:
        return len(self._elements)

    def __iter__(self) -> Iterator[str]:
        return iter(self._elements)

    def __contains__(self, value: object) -> bool:
        return value in self._elements

    @property
    def elements(self) -> FiniteSubSet[str]:
        """
        Get the elements.

        Returns
        -------
        FiniteSubSet[str]
            The elements.
        """
        return self._elements

    def meet_priority(self, generators: Iterable[Self]) -> int:
        """
        Compute the priority of an element considering an iterable of generators.

        This methods returns the negative value of the support.
        This ensures a traversal of the lattice in decreasing order of support.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The priority.

        """
        return -len(self)

    def join_priority(self, generators: Iterable[Self]) -> int:
        """
        Compute the priority of an element considering an iterable of generators.

        This methods returns the negative value of the cardinality of the intent.
        This ensures a reverse traversal of the lattice in decreasing order of
        the cardinality of the intent.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The priority.

        """
        return len(self)

    @property
    def closure(self) -> AbstractClosure[Self, str]:
        """
        Get the closure operator.

        Returns
        -------
        AbstractClosureEnumerable[str]
            The closure operator.

        """
        return self._connection.closures[0]


# pylint: disable=too-many-ancestors
class Intent(ClosedEnumerableMixin[Attribute], Element):  # type: ignore[misc]
    """
    It represents finite closed set of attributes.

    Parameters
    ----------
    connection
        The antitone Galois connection.
    elements
        The objects sharing the attributes in the closed set.

    """

    __slots__ = (
        "_hash",
        "_connection",
        "_elements",
    )

    def __init__(
        self,
        connection: GaloisConnection,
        elements: Iterable[str] | None = None,
    ) -> None:
        self._hash: int | None = None
        self._connection = connection
        objects = list(elements or [])
        self._elements = FiniteSubSet[Attribute](
            connection.context.universes[1],
            (
                attribute
                for attribute in connection.context.universes[1]
                if all((key, attribute) in connection.context for key in objects)
            ),
        )

    def __hash__(self) -> int:
        if self._hash is None:
            self._hash = hash((self._connection, self._elements))
        return self._hash

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __len__(self) -> int:
        return len(self._elements)

    def __iter__(self) -> Iterator[Attribute]:
        return iter(self._elements)

    def __contains__(self, value: object) -> bool:
        return value in self._elements

    @property
    def elements(self) -> FiniteSubSet[Attribute]:
        """
        Get the elements.

        Returns
        -------
        FiniteSubSet[Attribute]
            The elements.
        """
        return self._elements

    def meet_priority(self, generators: Iterable[Self]) -> int:
        """
        Compute the priority of an element considering an iterable of generators.

        This methods returns the negative value of the support.
        This ensures a traversal of the lattice in decreasing order of support.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The priority.

        """
        return -len(self)

    def join_priority(self, generators: Iterable[Self]) -> int:
        """
        Compute the priority of an element considering an iterable of generators.

        This methods returns the negative value of the cardinality of the intent.
        This ensures a reverse traversal of the lattice in decreasing order of
        the cardinality of the intent.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The priority.

        """
        return len(self)

    @property
    def closure(self) -> AbstractClosure[Self, Attribute]:
        """
        Get the closure operator.

        Returns
        -------
        AbstractClosureEnumerable[Attribute]
            The closure operator.

        """
        return self._connection.closures[1]


class ObjectPolarity:
    """
    It represents the polarity of object identifiers.
    """

    __slots__ = ("_connection",)

    def __init__(self, connection: GaloisConnection) -> None:
        self._connection = connection

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __call__(
        self,
        *closed: Intent,
        elements: Iterable[Attribute] | None = None,
    ) -> Extent:
        """
        Compute the correspondence of elements.

        Parameters
        ----------
        *closed
            A sequence of intents.
        elements
            An optional iterable of attributes.

        Returns
        -------
        Extent
            The correspondance of the elements.

        Raises
        ------
        ValueError
            If an element is not in the context co-domain.

        """
        return Extent(self._connection, itertools.chain(elements or [], *closed))

    @property
    def connection(self) -> GaloisConnection:
        """
        Get the connection.

        Returns
        -------
        GaloisConnection
            The connection.

        """
        return self._connection


class AttributePolarity:
    """
    It represents the polarity of attributes.
    """

    __slots__ = ("_connection",)

    def __init__(self, connection: GaloisConnection) -> None:
        self._connection = connection

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __call__(
        self,
        *closed: Extent,
        elements: Iterable[str] | None = None,
    ) -> Intent:
        """
        Compute the correspondence of elements.

        Parameters
        ----------
        *closed
            A sequence of extents.
        elements
            An optional iterable of identifiers.

        Returns
        -------
        Intent
            The correspondance of the elements.

        Raises
        ------
        ValueError
            If an element is not in the context domain.

        """
        return Intent(self._connection, itertools.chain(elements or [], *closed))

    @property
    def connection(self) -> GaloisConnection:
        """
        Get the connection.

        Returns
        -------
        GaloisConnection
            The connection.

        """
        return self._connection


class GaloisConnection(GaloisConnectionMixin[Extent, str, Intent, Attribute]):  # type: ignore[misc]
    # noinspection PyUnresolvedReferences,PyTypeChecker
    """
    It represents an antitone Galois connection.

    It is obtained using a context.

    Parameters
    ----------
    context
        The context.

    Examples
    --------
    >>> from galactic.algebras.concept import GaloisConnection
    >>> from galactic.algebras.examples.concept import context
    >>> attributes = context.co_domain
    >>> attributes
    <galactic.algebras.set.FiniteUniverse object at 0x...>
    >>> connection = GaloisConnection(context)
    >>> connection
    <galactic.algebras.concept.GaloisConnection object at 0x...>
    >>> list(
    ...     str(attribute)
    ...     for attribute in connection.polarities[0](
    ...         elements=["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"]
    ...     )
    ... )
    ['twolegs', 'feathers']
    >>> list(connection.closures[0](elements=["Dove", "Hen"]))
    ['Dove', 'Hen', 'Duck', 'Goose', 'Owl', 'Hawk']

    """

    __slots__ = (
        "_context",
        "_polarities",
    )

    def __init__(self, context: Context) -> None:
        self._context = context
        self._polarities = (AttributePolarity(self), ObjectPolarity(self))
        super().__init__()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @property
    def polarities(self) -> tuple[AttributePolarity, ObjectPolarity]:
        """
        Get the polarities.

        Returns
        -------
        tuple[AttributePolarity, ObjectPolarity]
            The polarities.

        """
        return self._polarities

    @property
    def context(self) -> Context:
        """
        Get the context.

        Returns
        -------
        Context
            The context

        """
        return self._context
