"""Family module."""

from collections.abc import Collection, Iterator, Mapping
from typing import cast

from galactic.algebras.closure import MooreFamilyEnumerableMixin
from galactic.algebras.lattice import (
    FiniteLatticeMixin,
    FrozenFiniteLattice,
    LatticeCoveringRelation,
)
from galactic.algebras.poset import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    FinitePartialOrder,
)
from galactic.helpers.core import default_repr

from ._concept import AbstractConceptLattice, Attributes, Objects
from ._connection import Extent, Intent


class ObjectExtents(Mapping[Extent, Objects]):
    """
    It represents a mapping from extent to object identifiers.

    Parameters
    ----------
    lattice
        A concept lattice.

    """

    __slots__ = ("_lattice",)

    def __init__(self, lattice: AbstractConceptLattice) -> None:
        self._lattice = lattice

    def __getitem__(self, extent: Extent) -> Objects:
        for concept, objects in self._lattice.object_concepts.items():
            if concept.extent == extent:
                return objects
        raise KeyError

    def __len__(self) -> int:
        return len(self._lattice.object_concepts)

    def __iter__(self) -> Iterator[Extent]:
        return (concept.extent for concept in self._lattice.object_concepts)


class AttributeIntents(Mapping[Intent, Attributes]):
    """
    It represents a mapping from intent to attributes.

    Parameters
    ----------
    lattice
        A concept lattice.

    """

    __slots__ = ("_lattice",)

    def __init__(self, lattice: AbstractConceptLattice) -> None:
        self._lattice = lattice

    def __getitem__(self, intent: Intent) -> Attributes:
        for concept, attributes in self._lattice.attribute_concepts.items():
            if concept.intent == intent:
                return attributes
        raise KeyError

    def __len__(self) -> int:
        return len(self._lattice.attribute_concepts)

    def __iter__(self) -> Iterator[Extent]:
        return (concept.intent for concept in self._lattice.attribute_concepts)


# pylint: disable=too-many-instance-attributes
class ExtentLattice(
    FiniteLatticeMixin[Extent],  # type: ignore[misc]
    MooreFamilyEnumerableMixin[Extent],  # type: ignore[misc]
):
    """
    Object Moore family.

    Parameters
    ----------
    lattice
        The concept lattice used to construct the object Moore family
    """

    __slots__ = (
        "_lattice",
        "_cover",
        "_order",
        "_top",
        "_bottom",
        "_meet_irreducible",
        "_join_irreducible",
        "_object_extents",
    )

    def __init__(self, lattice: AbstractConceptLattice) -> None:
        class Top:
            """
            It represents the top elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice.maximum is None:
                    return False
                return cast(bool, item == lattice.maximum.extent)

            def __len__(self) -> int:
                if lattice.maximum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[Extent]:
                if lattice.maximum is not None:
                    yield lattice.maximum.extent

        class Bottom:
            """
            It represents the bottom elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice.minimum is None:
                    return False
                return any(item == extent for extent in self)

            def __len__(self) -> int:
                if lattice.minimum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[Extent]:
                if lattice.minimum is not None:
                    yield lattice.minimum.extent

        class JoinIrreducible:
            """
            It represents the join elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice.minimum is None:
                    return False
                return any(item == extent for extent in self)

            def __len__(self) -> int:
                return len(lattice.join_irreducible)

            def __iter__(self) -> Iterator[Extent]:
                for element in lattice.join_irreducible:
                    yield element.extent

        class MeetIrreducible:
            """
            It represents the join elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice.minimum is None:
                    return False
                return any(item == extent for extent in self)

            def __len__(self) -> int:
                return len(lattice.meet_irreducible)

            def __iter__(self) -> Iterator[Extent]:
                for element in lattice.meet_irreducible:
                    yield element.extent

        self._lattice = lattice
        self._cover = LatticeCoveringRelation[Extent](self)
        self._order = FinitePartialOrder[Extent](self)
        self._top = Top()
        self._bottom = Bottom()
        self._join_irreducible = JoinIrreducible()
        self._meet_irreducible = MeetIrreducible()
        self._object_extents = ObjectExtents(lattice)

    def __len__(self) -> int:
        return len(self._lattice)

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number

        """
        return cast(int, self._lattice.version)

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[Extent]:
        """
        Get the partial order of this lattice.

        Returns
        -------
        AbstractFinitePartialOrder[Extent]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[Extent]:
        """
        Get the covering relation of this lattice.

        Returns
        -------
        AbstractFiniteCoveringRelation[Extent]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[Extent]:
        """
        Get the top elements.

        Returns
        -------
        Collection[Extent]
            The top elements.

        """
        return self._top

    @property
    def bottom(self) -> Collection[Extent]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[Extent]
            The bottom elements.

        """
        return self._bottom

    filter = FrozenFiniteLattice.filter
    ideal = FrozenFiniteLattice.ideal

    # AbstractFiniteJoinSemiLattice properties and methods

    @property
    def join_irreducible(self) -> AbstractFinitePartiallyOrderedSet[Extent]:
        """
        Get the join-irreducible elements of this lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[Extent]
            The join-irreducible elements.

        """
        # noinspection PyTypeChecker
        return self._join_irreducible

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    def meet_irreducible(self) -> AbstractFinitePartiallyOrderedSet[Extent]:
        """
        Get the meet-irreducible elements of this lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[Extent]
            The meet-irreducible elements.

        """
        # noinspection PyTypeChecker
        return self._meet_irreducible

    @property
    def object_extents(self) -> ObjectExtents:
        """
        Get the object extents mapping.

        Returns
        -------
        ObjectExtents
            The object extents mapping.

        """
        return self._object_extents


# pylint: disable=too-many-instance-attributes
class IntentLattice(
    FiniteLatticeMixin[Intent],  # type: ignore[misc]
    MooreFamilyEnumerableMixin[Intent],  # type: ignore[misc]
):
    """
    Attribute Moore family.

    Parameters
    ----------
    lattice
        The concept lattice used to construct the attribute Moore family
    """

    __slots__ = (
        "_lattice",
        "_cover",
        "_order",
        "_top",
        "_bottom",
        "_meet_irreducible",
        "_join_irreducible",
        "_attribute_intents",
    )

    def __init__(self, lattice: AbstractConceptLattice) -> None:
        class Top:
            """
            It represents the top elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice.minimum is None:
                    return False
                return cast(bool, item == lattice.minimum.intent)

            def __len__(self) -> int:
                if lattice.minimum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[Intent]:
                if lattice.minimum is not None:
                    yield lattice.minimum.intent

        class Bottom:
            """
            It represents the bottom elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice.maximum is None:
                    return False
                return any(item == intent for intent in self)

            def __len__(self) -> int:
                if lattice.maximum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[Intent]:
                if lattice.maximum is not None:
                    yield lattice.maximum.intent

        class JoinIrreducible:
            """
            It represents the join elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice.maximum is None:
                    return False
                return any(item == intent for intent in self)

            def __len__(self) -> int:
                return len(lattice.meet_irreducible)

            def __iter__(self) -> Iterator[Intent]:
                for element in lattice.meet_irreducible:
                    yield element.intent

        class MeetIrreducible:
            """
            It represents the join elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice.maximum is None:
                    return False
                return any(item == intent for intent in self)

            def __len__(self) -> int:
                return len(lattice.join_irreducible)

            def __iter__(self) -> Iterator[Intent]:
                for element in lattice.join_irreducible:
                    yield element.intent

        self._lattice = lattice
        self._cover = LatticeCoveringRelation[Intent](self)
        self._order = FinitePartialOrder[Intent](self)
        self._top = Top()
        self._bottom = Bottom()
        self._join_irreducible = JoinIrreducible()
        self._meet_irreducible = MeetIrreducible()
        self._attribute_intents = AttributeIntents(lattice)

    def __len__(self) -> int:
        return len(self._lattice)

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number

        """
        return cast(int, self._lattice.version)

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[Intent]:
        """
        Get the partial order of this lattice.

        Returns
        -------
        AbstractFinitePartialOrder[Intent]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[Intent]:
        """
        Get the covering relation of this lattice.

        Returns
        -------
        AbstractFiniteCoveringRelation[Intent]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[Intent]:
        """
        Get the top elements.

        Returns
        -------
        Collection[Intent]
            The top elements.

        """
        return self._top

    @property
    def bottom(self) -> Collection[Intent]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[Intent]
            The bottom elements.

        """
        return self._bottom

    filter = FrozenFiniteLattice.filter
    ideal = FrozenFiniteLattice.ideal

    # AbstractFiniteJoinSemiLattice properties and methods

    @property
    def join_irreducible(self) -> AbstractFinitePartiallyOrderedSet[Intent]:
        """
        Get the join-irreducible elements of this lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[Intent]
            The join-irreducible elements.

        """
        # noinspection PyTypeChecker
        return self._join_irreducible

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    def meet_irreducible(self) -> AbstractFinitePartiallyOrderedSet[Intent]:
        """
        Get the meet-irreducible elements of this lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[Intent]
            The meet-irreducible elements.

        """
        # noinspection PyTypeChecker
        return self._meet_irreducible

    @property
    def attribute_intents(self) -> AttributeIntents:
        """
        Get the attribute concepts mapping.

        Returns
        -------
        AttributeIntents
            The attribute intents mapping.

        """
        return self._attribute_intents
