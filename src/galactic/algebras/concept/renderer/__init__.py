"""
The :mod:`galactic.algebras.concept.renderer` module.

It defines:

* :class:`ConceptRenderer` for the rendering of concepts;
* :class:`ExtentRenderer` for the rendering of extents;
* :class:`IntentRenderer` for the rendering of intents;
* :class:`HierarchicalRenderer` fot the rendering of edges between concepts.
"""

from ._renderer import (
    ConceptRenderer,
    ExtentRenderer,
    HierarchicalRenderer,
    IntentRenderer,
)

__all__ = (
    "ConceptRenderer",
    "ExtentRenderer",
    "IntentRenderer",
    "HierarchicalRenderer",
)
