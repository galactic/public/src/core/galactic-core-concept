"""Concept renderer module."""

from __future__ import annotations

from typing import TYPE_CHECKING

import graphviz

from galactic.algebras.concept import (
    AbstractConceptLattice,
    Concept,
    Extent,
    ExtentLattice,
    Intent,
    IntentLattice,
)
from galactic.algebras.relational.renderer import EdgeRenderer, NodeRenderer

if TYPE_CHECKING:
    from collections.abc import Collection


# pylint: disable=too-few-public-methods
class HtmlAttributes:
    """
    It represents HTML attributes for graphviz.

    Parameters
    ----------
    elements
        A dictionary of attributes.

    """

    __slots__ = ("_elements",)

    def __init__(self, elements: dict[str, str] | None = None) -> None:
        if elements is None:
            elements = {}
        self._elements = elements

    def __str__(self) -> str:
        return " ".join(f'{key}="{value}"' for key, value in self._elements.items())


class HtmlTable:
    """
    It represents HTML tables in graphviz.

    Parameters
    ----------
    attrs
        A dictionary of attributes.
    """

    __slots__ = ("_attributes", "_rows")

    def __init__(self, attrs: dict[str, str] | None = None) -> None:
        if attrs is None:
            self._attributes = HtmlAttributes(
                {
                    "BORDER": "0",
                    "CELLBORDER": "1",
                    "CELLSPACING": "0",
                    "CELLPADDING": "5",
                },
            )
        else:
            self._attributes = HtmlAttributes(attrs)
        self._rows: list[str] = []

    def __len__(self) -> int:
        return len(self._rows)

    def add(
        self,
        attrs: dict[str, str] | None = None,
        content: str = "",
    ) -> None:
        """
        Add a row to the HTML table.

        Parameters
        ----------
        attrs
            A dictionary of attributes
        content
            The row content.

        """
        self._rows.append(
            f"<TR><TD {HtmlAttributes(attrs)}>{content}</TD></TR>",
        )

    def __str__(self) -> str:
        return f"<<TABLE {self._attributes}>{''.join(self._rows)}</TABLE>>"


# pylint: disable=too-few-public-methods, too-many-instance-attributes
class ConceptRenderer(NodeRenderer[Concept, Concept]):  # type: ignore[misc]
    """
    It renders graphviz nodes.

    Parameters
    ----------
    lattice
        The concept lattice.
    fill_color
        The fill color.
    anonymous_color
        The anonymous color.
    attribute_color
        The attribute color.
    object_color
        The object color.
    meet_irreducible
        The flag for rendering meet irreducible in a binary table.
    join_irreducible
        The flag for rendering join irreducible in a binary table.

    Notes
    -----
        See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = (
        "_lattice",
        "_fill_color",
        "_anonymous_color",
        "_attribute_color",
        "_object_color",
        "_meet_irreducible",
        "_join_irreducible",
        "_compact",
    )

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def __init__(
        self,
        lattice: AbstractConceptLattice,
        fill_color: str = "#ffffff",
        anonymous_color: str = "#d3d3d3",
        attribute_color: str = "#fcaf3e",
        object_color: str = "#8ae234",
        meet_irreducible: bool = False,
        join_irreducible: bool = False,
        compact: bool = True,
    ) -> None:
        self._lattice = lattice
        self._fill_color = fill_color
        self._anonymous_color = anonymous_color
        self._attribute_color = attribute_color
        self._object_color = object_color
        self._meet_irreducible = meet_irreducible
        self._join_irreducible = join_irreducible
        self._compact = compact

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def attributes(
        self,
        element: Concept,
        index: int | None = None,
        current: bool = False,
        successors: Collection[Concept] | None = None,
        predecessors: Collection[Concept] | None = None,
    ) -> dict[str, str]:
        """
        Return a dictionary of graphviz attributes for a node.

        Parameters
        ----------
        element
            The element to render.
        index
            The element index.
        current
            is ``element`` the current element?
        successors
            The successors of the element.
        predecessors
            The predecessors of the element.

        Returns
        -------
        dict[str, str]
            A dictionary of graphviz attributes.

        """
        html_table = HtmlTable()
        if element in self._lattice.object_concepts:
            html_table.add({"ALIGN": "CENTER"}, f"${index}: #{len(element.extent)}")
            if element in self._lattice.attribute_concepts:
                self._add_attributes(element, html_table)
            self._add_objects(element, html_table)
        elif element in self._lattice.attribute_concepts:
            html_table.add({"ALIGN": "CENTER"}, f"${index}: #{len(element.extent)}")
            self._add_attributes(element, html_table)

        if len(html_table) > 0:
            return {
                "label": str(html_table),
                "shape": "none",
                "style": "filled",
                "fillcolor": self._fill_color,
                "width": "0",
                "height": "0",
                "margin": "0,0",
            }
        return {
            "label": f"${index}: #{len(element.extent)}",
            "shape": "circle",
            "style": "filled",
            "width": "0",
            "height": "0",
            "margin": "0,0",
        }

    def _add_attributes(self, element: Concept, html_table: HtmlTable) -> None:
        if element in self._lattice.meet_irreducible:
            attributes = {
                "BGCOLOR": self._attribute_color,
                "ALIGN": "LEFT",
                "BALIGN": "LEFT",
            }
        else:
            attributes = {
                "ALIGN": "LEFT",
                "BALIGN": "LEFT",
            }
        html_table.add(
            attributes,
            "<BR/>".join(
                graphviz.escape(
                    repr(
                        (
                            attribute.__name__
                            if hasattr(attribute, "__name__")
                            else str(attribute)
                        ),
                    ),
                )
                for attribute in self._lattice.attribute_concepts[element]
            ),
        )

    def _add_objects(self, element: Concept, html_table: HtmlTable) -> None:
        if element in self._lattice.join_irreducible:
            html_attributes = {
                "BGCOLOR": self._object_color,
                "ALIGN": "LEFT",
                "BALIGN": "LEFT",
            }
        else:
            html_attributes = {
                "ALIGN": "LEFT",
                "BALIGN": "LEFT",
            }
        html_table.add(
            html_attributes,
            "<BR/>".join(
                graphviz.escape(repr(key))
                for key in self._lattice.object_concepts[element]
            ),
        )

    # pylint: disable=too-many-return-statements
    def render(self, element: Concept, index: int) -> str:
        """
        Render a cell in the reduced context.

        Parameters
        ----------
        element
            The concept to render
        index
            The concept index

        Returns
        -------
        str
            The cell rendered.

        """
        if self._join_irreducible:
            objects = self._lattice.object_concepts[element]
            if self._compact:
                first_object = next(iter(objects))
                if len(objects) > 1:
                    return f"{first_object}, ... ({len(objects)})"
                return first_object
            return ", ".join(objects)
        if self._meet_irreducible:
            attributes = self._lattice.attribute_concepts[element]
            if self._compact:
                first_attribute = next(iter(attributes))
                if len(attributes) > 1:
                    return f"{first_attribute}, ... ({len(attributes)})"
                return str(first_attribute)
            return ", ".join(str(attribute) for attribute in attributes)
        return str(index)


class ExtentRenderer(NodeRenderer[Extent, Extent]):  # type: ignore[misc]
    """
    It renders graphviz nodes.

    Parameters
    ----------
    lattice
        The extent lattice.
    fill_color
        The fill color.
    object_color
        The object color.

    Notes
    -----
        See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = (
        "_lattice",
        "_fill_color",
        "_object_color",
    )

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def __init__(
        self,
        lattice: ExtentLattice,
        fill_color: str = "#ffffff",
        object_color: str = "#8ae234",
    ) -> None:
        self._lattice = lattice
        self._fill_color = fill_color
        self._object_color = object_color

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def attributes(
        self,
        element: Extent,
        index: int | None = None,
        current: bool = False,
        successors: Collection[Extent] | None = None,
        predecessors: Collection[Extent] | None = None,
    ) -> dict[str, str]:
        """
        Return a dictionary of graphviz attributes for a node.

        Parameters
        ----------
        element
            The element to render.
        index
            The element index.
        current
            is ``element`` the current element?
        successors
            The successors of the element.
        predecessors
            The predecessors of the element.

        Returns
        -------
        dict[str, str]
            A dictionary of graphviz attributes.

        """
        html_table = HtmlTable()
        if element in self._lattice.object_extents:
            html_table.add({"ALIGN": "CENTER"}, f"${index}: #{len(element)}")
            self._add_objects(element, html_table)

        if len(html_table) > 0:
            return {
                "label": str(html_table),
                "shape": "none",
                "style": "filled",
                "fillcolor": self._fill_color,
                "width": "0",
                "height": "0",
                "margin": "0,0",
            }
        return {
            "label": f"${index}: #{len(element)}",
            "shape": "circle",
            "style": "filled",
            "width": "0",
            "height": "0",
            "margin": "0,0",
        }

    def _add_objects(self, element: Extent, html_table: HtmlTable) -> None:
        if element in self._lattice.join_irreducible:
            html_attributes = {
                "BGCOLOR": self._object_color,
                "ALIGN": "LEFT",
                "BALIGN": "LEFT",
            }
        else:
            html_attributes = {
                "ALIGN": "LEFT",
                "BALIGN": "LEFT",
            }
        html_table.add(
            html_attributes,
            "<BR/>".join(
                graphviz.escape(repr(key))
                for key in self._lattice.object_extents[element]
            ),
        )


class IntentRenderer(NodeRenderer[Intent, Intent]):  # type: ignore[misc]
    """
    It renders graphviz nodes.

    Parameters
    ----------
    lattice
        The intent lattice.
    fill_color
        The fill color.
    attribute_color
        The attribute color.

    Notes
    -----
        See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = (
        "_lattice",
        "_fill_color",
        "_attribute_color",
    )

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def __init__(
        self,
        lattice: IntentLattice,
        fill_color: str = "#ffffff",
        attribute_color: str = "#fcaf3e",
    ) -> None:
        self._lattice = lattice
        self._fill_color = fill_color
        self._attribute_color = attribute_color

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def attributes(
        self,
        element: Intent,
        index: int | None = None,
        current: bool = False,
        successors: Collection[Intent] | None = None,
        predecessors: Collection[Intent] | None = None,
    ) -> dict[str, str]:
        """
        Return a dictionary of graphviz attributes for a node.

        Parameters
        ----------
        element
            The element to render.
        index
            The element index.
        current
            is ``element`` the current element?
        successors
            The successors of the element.
        predecessors
            The predecessors of the element.

        Returns
        -------
        dict[str, str]
            A dictionary of graphviz attributes.

        """
        html_table = HtmlTable()
        if element in self._lattice.attribute_intents:
            html_table.add({"ALIGN": "CENTER"}, f"${index}: #{len(element)}")
            self._add_objects(element, html_table)

        if len(html_table) > 0:
            return {
                "label": str(html_table),
                "shape": "none",
                "style": "filled",
                "fillcolor": self._fill_color,
                "width": "0",
                "height": "0",
                "margin": "0,0",
            }
        return {
            "label": f"${index}: #{len(element)}",
            "shape": "circle",
            "style": "filled",
            "width": "0",
            "height": "0",
            "margin": "0,0",
        }

    def _add_objects(self, element: Intent, html_table: HtmlTable) -> None:
        if element in self._lattice.join_irreducible:
            html_attributes = {
                "BGCOLOR": self._attribute_color,
                "ALIGN": "LEFT",
                "BALIGN": "LEFT",
            }
        else:
            html_attributes = {
                "ALIGN": "LEFT",
                "BALIGN": "LEFT",
            }
        html_table.add(
            html_attributes,
            "<BR/>".join(
                graphviz.escape(repr(str(attribute)))
                for attribute in self._lattice.attribute_intents[element]
            ),
        )


class HierarchicalRenderer(EdgeRenderer[Concept, Concept]):  # type: ignore[misc]
    """
    It renders edge attributes.

    Compression is used to determine the minimum edge length between two consecutive
    concepts by analyzing the difference in the lengths of their extents, eventually
    using the relative difference with other predecessors and successors.

    Parameters
    ----------
    compression
        The compression rate
    relative
        The relative flag

    Notes
    -----
    See http://www.graphviz.org/doc/info/attrs.html.
    """

    __slots__ = ("_compression", "_relative")

    def __init__(
        self,
        compression: float | None = None,
        relative: bool = False,
    ) -> None:
        if compression is not None:
            compression = max(0, min(1, compression))
        self._compression = compression
        self._relative = relative

    def attributes(
        self,
        source: Concept,
        destination: Concept,
        successors: Collection[Concept] | None = None,
        predecessors: Collection[Concept] | None = None,
    ) -> dict[str, str]:
        """
        Return a dictionary of graphviz attributes for an edge.

        Parameters
        ----------
        source
            The edge source
        destination
            The edge destination
        successors
            The successors of source (including current destination).
        predecessors
            The predecessors of destination (including current source).

        Returns
        -------
        dict[str, str]
            A dictionary of graphviz attributes
        """
        if self._compression is None or self._compression == 1:
            return {}
        if self._relative:
            difference = len(destination.extent) - len(source.extent)

            if successors:
                difference_successors = min(
                    len(successor.extent) for successor in successors
                ) - len(source.extent)
            else:
                difference_successors = difference

            if predecessors:
                difference_predecessors = len(destination.extent) - min(
                    len(predecessor.extent) for predecessor in predecessors
                )
            else:
                difference_predecessors = difference

            minimum = min(difference_successors, difference_predecessors)
            if minimum == 0:
                length = 1
            else:
                ratio = difference / minimum
                length = max(1, round(ratio * (1 - self._compression)))
        else:
            difference = len(destination.extent) - len(source.extent)
            length = max(1, round(difference * (1 - self._compression)))
        return {"minlen": str(length)}

    @property
    def compression(self) -> float | None:
        """
        Get the compression rate.

        Returns
        -------
        float | None
            The compression rate.
        """
        return self._compression

    @property
    def relative(self) -> bool:
        """
        Get the relative flag.

        Returns
        -------
        bool
            The relative flag.
        """
        return self._relative
