from collections.abc import Collection

from galactic.algebras.concept import (
    AbstractConceptLattice,
    Concept,
    Extent,
    ExtentLattice,
    Intent,
    IntentLattice,
)
from galactic.algebras.relational.renderer import (
    EdgeRenderer,
    NodeRenderer,
)

class ConceptRenderer(NodeRenderer[Concept, Concept]):  # type: ignore[misc]
    def __init__(
        self,
        lattice: AbstractConceptLattice,
        fill_color: str = "#ffffff",
        anonymous_color: str = "#d3d3d3",
        attribute_color: str = "#fcaf3e",
        object_color: str = "#8ae234",
        meet_irreducible: bool = False,
        join_irreducible: bool = False,
        compact: bool = True,
    ) -> None: ...

    # pylint: disable=too-many-arguments
    def attributes(
        self,
        element: Concept,
        index: int | None = None,
        current: bool = False,
        successors: Collection[Concept] | None = None,
        predecessors: Collection[Concept] | None = None,
    ) -> dict[str, str]: ...
    def render(self, element: Concept, index: int) -> str: ...

class ExtentRenderer(NodeRenderer[Extent, Extent]):  # type: ignore[misc]
    def __init__(
        self,
        lattice: ExtentLattice,
        fill_color: str = "#ffffff",
        object_color: str = "#8ae234",
    ) -> None: ...
    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def attributes(
        self,
        element: Extent,
        index: int | None = None,
        current: bool = False,
        successors: Collection[Extent] | None = None,
        predecessors: Collection[Extent] | None = None,
    ) -> dict[str, str]: ...

class IntentRenderer(NodeRenderer[Intent, Intent]):  # type: ignore[misc]
    def __init__(
        self,
        lattice: IntentLattice,
        fill_color: str = "#ffffff",
        attribute_color: str = "#fcaf3e",
    ) -> None: ...
    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def attributes(
        self,
        element: Intent,
        index: int | None = None,
        current: bool = False,
        successors: Collection[Intent] | None = None,
        predecessors: Collection[Intent] | None = None,
    ) -> dict[str, str]: ...

class HierarchicalRenderer(EdgeRenderer[Concept, Concept]):  # type: ignore[misc]
    def __init__(
        self,
        compression: float | None = None,
        relative: bool = False,
    ) -> None: ...
    def attributes(
        self,
        source: Concept,
        destination: Concept,
        successors: Collection[Concept] | None = None,
        predecessors: Collection[Concept] | None = None,
    ) -> dict[str, str]: ...
    @property
    def compression(self) -> float | None: ...
    @property
    def relative(self) -> bool: ...
