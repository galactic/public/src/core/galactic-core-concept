"""Attribute module."""

from __future__ import annotations

import functools
from collections.abc import Callable

# noinspection PyCompatibility
Attribute = Callable[[object], bool | None]


def member(
    key: str,
    str_func: Callable[[Attribute], str] | None = None,
    repr_func: Callable[[Attribute], str] | None = None,
) -> Attribute:
    """
    Compute a member attribute from a key.

    Parameters
    ----------
    key
        The key to be searched
    str_func
        The string function
    repr_func
        The representation function

    Returns
    -------
    Attribute
        The created attribute

    """

    def create_attribute(
        string_func: Callable[[Attribute], str],
        representation_func: Callable[[Attribute], str],
    ) -> Callable[[Attribute], Attribute]:
        """
        Wrap an attribute.

        Parameters
        ----------
        string_func
            The string function
        representation_func
            The representation function

        Returns
        -------
        Callable[[Attribute], Attribute]
            The wrapped function.

        """

        class AttributeWrapper:
            """
            It wraps functions in a compact way when calling :func:`repr`.
            """

            def __init__(self, func: Attribute) -> None:
                self._func = func
                functools.update_wrapper(self, func)

            def __call__(self, value: object) -> bool | None:
                return self._func(value)

            def __repr__(self) -> str:
                return representation_func(self._func)

            def __str__(self) -> str:
                return string_func(self._func)

        def _wrap(func: Attribute) -> Attribute:
            return AttributeWrapper(func)

        return _wrap

    def attribute(value: object) -> bool | None:
        """
        Compute the attribute membership.

        Parameters
        ----------
        value
            The object

        Returns
        -------
        bool | None
            The membership value of the attribute.

        """
        try:
            return key in value  # type: ignore[operator]
        except (ValueError, TypeError):
            return None

    attribute.__name__ = attribute.__qualname__ = key
    return create_attribute(
        lambda _: key if str_func is None else str_func,  # type: ignore[return-value,arg-type]
        repr if repr_func is None else repr_func,
    )(attribute)


def relation(
    key: str,
    index: int,
    str_func: Callable[[Attribute], str] | None = None,
    repr_func: Callable[[Attribute], str] | None = None,
) -> Attribute:
    """
    Compute a relation attribute from an index and a key.

    Parameters
    ----------
    index
        The index whose truth value is got
    key
        The attribute name
    str_func
        The string function
    repr_func
        The representation function

    Returns
    -------
    Attribute
        The created attribute

    """

    def create_attribute(
        string_func: Callable[[Attribute], str],
        representation_func: Callable[[Attribute], str],
    ) -> Callable[[Attribute], Attribute]:
        """
        Wrap an attribute.

        Parameters
        ----------
        string_func
            The string function
        representation_func
            The representation function

        Returns
        -------
        Callable[[Attribute], Attribute]
            The wrapped function.

        """

        class AttributeWrapper:
            """
            It wraps functions in a compact way when calling :func:`repr`.
            """

            def __init__(self, func: Attribute) -> None:
                self._func = func
                functools.update_wrapper(self, func)

            def __call__(self, value: object) -> bool | None:
                return self._func(value)

            def __repr__(self) -> str:
                return representation_func(self._func)

            def __str__(self) -> str:
                return string_func(self._func)

        def _wrap(func: Attribute) -> Attribute:
            return AttributeWrapper(func)

        return _wrap

    def attribute(value: object) -> bool | None:
        """
        Compute the attribute relation.

        Parameters
        ----------
        value
            The object

        Returns
        -------
        bool | None
            The relation value of the attribute.

        """
        try:
            return bool(value[index])  # type: ignore[index]
        except (ValueError, TypeError, IndexError):
            return None

    attribute.__name__ = attribute.__qualname__ = key
    return create_attribute(
        lambda _: key if str_func is None else str_func,  # type: ignore[return-value,arg-type]
        repr if repr_func is None else repr_func,
    )(attribute)
