"""Context module."""

from __future__ import annotations

from collections.abc import (
    Collection,
    Iterable,
    Iterator,
    Mapping,
    Sequence,
)
from typing import (
    Any,
    Protocol,
    cast,
    runtime_checkable,
)

from galactic.algebras.relational import (
    AbstractBinaryRelation,
    FiniteRelationMixin,
)
from galactic.algebras.set import FiniteUniverse, FrozenFIFOSet
from galactic.helpers.core import default_repr
from galactic.helpers.population import Population

from ._attribute import Attribute, member, relation


@runtime_checkable
class AbstractContext(
    AbstractBinaryRelation[str, Attribute],  # type: ignore[misc]
    Protocol,
):
    """
    An abstract context is a binary relation between strings and attributes.
    """


# noinspection PyUnresolvedReferences
class Context(FiniteRelationMixin):  # type: ignore[misc]
    """
    It represents finite context in classical FCA.

    Parameters
    ----------
    objects
        A population.
    attributes :
        An iterable of attribute

    Raises
    ------
    ValueError
        If the context is empty.

    Examples
    --------
    >>> from pprint import pprint
    >>> from galactic.algebras.examples.concept import animals, context
    >>> context
    <galactic.algebras.concept.Context object at 0x...>
    >>> attributes = list(context.co_domain)
    >>> pprint(list(context.domain))
    ['Dove',
     'Hen',
     'Duck',
     'Goose',
     'Owl',
     'Hawk',
     'Eagle',
     'Fox',
     'Dog',
     'Wolf',
     'Cat',
     'Tiger',
     'Lion',
     'Horse',
     'Zebra',
     'Cow']
    >>> pprint([attribute for attribute in context.co_domain])
    [<function small at 0x...>,
     <function twolegs at 0x...>,
     <function feathers at 0x...>,
     <function fly at 0x...>,
     <function swim at 0x...>,
     <function hunt at 0x...>,
     <function medium at 0x...>,
     <function fourlegs at 0x...>,
     <function hair at 0x...>,
     <function run at 0x...>,
     <function mane at 0x...>,
     <function big at 0x...>,
     <function hooves at 0x...>]
    >>> pprint([str(attribute) for attribute in context.co_domain])
    ['small',
     'twolegs',
     'feathers',
     'fly',
     'swim',
     'hunt',
     'medium',
     'fourlegs',
     'hair',
     'run',
     'mane',
     'big',
     'hooves']
    >>> pprint([successor for successor in context.successors("Dove")])
    [<function small at 0x...>,
     <function twolegs at 0x...>,
     <function feathers at 0x...>,
     <function fly at 0x...>]
    >>> pprint([str(successor) for successor in context.successors("Dove")])
    ['small', 'twolegs', 'feathers', 'fly']
    >>> attributes[0]
    <function small at 0x...>
    >>> str(attributes[0])
    'small'
    >>> pprint(
    ...     [predecessor for predecessor in context.predecessors(attributes[0])]
    ... )
    ['Dove', 'Hen', 'Duck', 'Goose', 'Owl', 'Hawk', 'Cat']

    """

    __slots__ = (
        "_objects",
        "_attributes",
        "_names",
        "_universes",
        "_length",
    )

    def __init__(
        self,
        objects: Population,
        attributes: Iterable[Attribute],
    ) -> None:
        self._objects = objects
        self._attributes = FrozenFIFOSet(attributes)
        self._names: dict[str, Attribute] = {
            str(attribute): attribute for attribute in self._attributes
        }
        self._universes = (
            FiniteUniverse(self._objects),
            FiniteUniverse(self._attributes),
        )
        self._length: int | None = None

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __contains__(self, value: object) -> bool:
        if isinstance(value, tuple) and len(value) == 2:
            key, attribute = value
            return (
                key in self._objects
                and attribute in self._attributes
                and attribute(self._objects[key])
            )
        return False

    def __len__(self) -> int:
        if self._length is None:
            self._length = sum(1 for _ in self)
        return self._length

    def __iter__(self) -> Iterator[tuple[str, Attribute]]:
        for key in self._objects:
            for attribute in self._attributes:
                if attribute(self._objects[key]):
                    yield key, attribute

    @property
    def universes(self) -> tuple[FiniteUniverse[str], FiniteUniverse[Attribute]]:
        """
        Get the universes of this context.

        Returns
        -------
        tuple[FiniteUniverse[str], FiniteUniverse[Attribute]]
            The universes.

        """
        return self._universes

    @property
    def domain(self) -> FiniteUniverse[str]:
        """
        Get the domain of this context.

        Returns
        -------
        FiniteUniverse[str]
            The domain.

        """
        return self._universes[0]

    @property
    def co_domain(self) -> FiniteUniverse[Attribute]:
        """
        Get the co-domain of this context.

        Returns
        -------
        FiniteUniverse[Attribute]
            The co-domain.

        """
        return self._universes[1]

    def successors(self, element: str) -> Collection[Attribute]:
        """
        Compute the successors of element.

        Parameters
        ----------
        element
            The object key (or identifier).

        Returns
        -------
        Collection[Attribute]
            The successors of element.

        Raises
        ------
        ValueError
            If element is not in the domain.

        """
        if element not in self._objects:
            raise ValueError(f"{element} is not in the domain")
        return Successors(self._objects[element], self._attributes)

    def predecessors(self, element: Attribute) -> Collection[str]:
        """
        Compute the successors of key.

        Parameters
        ----------
        element
            The attribute.

        Returns
        -------
        Collection[str]
            The predecessors of element.

        Raises
        ------
        ValueError
            If element is not in the co-domain.

        """
        if element not in self._attributes:
            raise ValueError(f"{element} is not in the co-domain")
        return Predecessors(element, self._objects)

    def attribute(self, name: str) -> Attribute:
        """
        Get an attribute by its name.

        Parameters
        ----------
        name
            The parameter name

        Returns
        -------
        Attribute
            The attribute

        """
        return self._names[name]


class Successors(Collection[Attribute]):
    """
    It implements the successors of an object identifier.

    Parameters
    ----------
    data
        Any python value
    attributes :
        The attributes of the context.

    """

    __slots__ = ("_data", "_attributes", "_length")

    def __init__(self, data: object, attributes: FrozenFIFOSet[Attribute]) -> None:
        self._data = data
        self._attributes = attributes
        self._length: int | None = None

    def __repr__(self) -> str:
        return cast(str, default_repr(self, long=False))

    # noinspection PyCallingNonCallable
    def __contains__(self, value: object) -> bool:
        return value in self._attributes and value(self._data)  # type: ignore[operator]

    def __len__(self) -> int:
        if self._length is None:
            self._length = sum(
                1 for attribute in self._attributes if attribute(self._data)
            )
        return self._length

    def __iter__(self) -> Iterator[Attribute]:
        return (attribute for attribute in self._attributes if attribute(self._data))


class Predecessors(Collection[str]):
    """
    It implements the predecessors of an attribute.

    Parameters
    ----------
    attribute
        An attribute
    objects
        The objects of the context.

    """

    __slots__ = ("_attribute", "_objects", "_length")

    def __init__(self, attribute: Attribute, objects: Population) -> None:
        self._attribute = attribute
        self._objects = objects
        self._length: int | None = None

    def __repr__(self) -> str:
        return cast(str, default_repr(self, long=False))

    def __contains__(self, value: object) -> bool:
        return value in self._objects and self._attribute(self._objects[value]) is True

    def __len__(self) -> int:
        if self._length is None:
            self._length = sum(
                1 for data in self._objects.values() if self._attribute(data)
            )
        return self._length

    def __iter__(self) -> Iterator[str]:
        return (key for key, data in self._objects.items() if self._attribute(data))


# noinspection PyUnresolvedReferences
def create_context_from_data(
    objects: Sequence[Sequence[str]] | Mapping[str, Sequence[str]],
) -> Context:
    """
    Create a context from data.

    Parameters
    ----------
    objects
        A dictionnary of objects.

    Returns
    -------
    Context
        A context created from the objects

    Raises
    ------
    TypeError
        If the objects are malformed.

    Examples
    --------
    >>> from galactic.algebras.concept import create_context_from_data
    >>> data = {
    ...     "0": ["composite", "even", "square"],
    ...     "1": ["odd", "square"],
    ...     "2": ["even", "prime"],
    ...     "3": ["odd", "prime"],
    ...     "4": ["composite", "even", "square"],
    ...     "5": ["odd", "prime"],
    ...     "6": ["composite", "even"],
    ...     "7": ["odd", "prime"],
    ...     "8": ["composite", "even"],
    ...     "9": ["composite", "odd", "square"],
    ... }
    >>> context = create_context_from_data(data)
    >>> context
    <galactic.algebras.concept.Context object at 0x...>
    >>> context.domain
    <galactic.algebras.set.FiniteUniverse object at 0x...>
    >>> context.co_domain
    <galactic.algebras.set.FiniteUniverse object at 0x...>
    >>> list(context.domain)
    ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    >>> list(str(attribute) for attribute in context.co_domain)
    ['composite', 'even', 'square', 'odd', 'prime']
    >>> ("0", context.co_domain[0]) in context
    True
    >>> list(context.predecessors(context.co_domain[0]))
    ['0', '4', '6', '8', '9']

    """
    attributes: dict[str, Attribute] = {}
    if isinstance(objects, Mapping):
        for value in objects.values():
            for element in value:
                if isinstance(element, str):
                    if element not in attributes:
                        attributes[element] = member(element)
                else:
                    raise TypeError(f"{element} is not a string")
    else:
        for value in objects:
            for element in value:
                if isinstance(element, str):
                    if element not in attributes:
                        attributes[element] = member(element)
                else:
                    raise TypeError(f"{element} is not a string")
    return Context(Population(objects), list(attributes.values()))


def create_context_from_table(
    objects: Sequence[Sequence[Any]] | Mapping[str, Sequence[Any]],
    attributes: Sequence[str],
) -> Context:
    """
    Create a context from a table.

    Parameters
    ----------
    objects
        A dictionnary of objects.
    attributes : Sequence[str]
        A sequence of strings.

    Returns
    -------
    Context
        A context created from the objects and the attributes

    Examples
    --------
    >>> from galactic.algebras.concept import create_context_from_table
    >>> attributes = ["composite", "even", "square", "odd", "prime"]
    >>> objects = {
    ...     "0": [True, True, True, False, False],
    ...     "1": [False, False, True, True, False],
    ...     "2": [False, True, False, False, True],
    ...     "3": [False, False, False, True, True],
    ...     "4": [True, True, True, False, False],
    ...     "5": [False, False, False, True, True],
    ...     "6": [True, True, False, False, False],
    ...     "7": [False, False, False, True, True],
    ...     "8": [True, True, False, False, False],
    ...     "9": [True, False, True, True, False],
    ... }
    >>> context = create_context_from_table(objects, attributes)
    >>> context
    <galactic.algebras.concept.Context object at 0x...>
    >>> context.domain
    <galactic.algebras.set.FiniteUniverse object at 0x...>
    >>> context.co_domain
    <galactic.algebras.set.FiniteUniverse object at 0x...>
    >>> list(context.domain)
    ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    >>> list(str(attribute) for attribute in context.co_domain)
    ['composite', 'even', 'square', 'odd', 'prime']
    >>> ("0", context.co_domain[0]) in context
    True
    >>> list(context.predecessors(context.co_domain[0]))
    ['0', '4', '6', '8', '9']

    """
    return Context(
        Population(objects),
        (relation(attribute, index) for index, attribute in enumerate(attributes)),
    )
