"""Concept module."""

from __future__ import annotations

import copy
import itertools
from abc import abstractmethod
from collections import defaultdict
from collections.abc import Collection, Iterable, Iterator, Mapping
from typing import Protocol, cast

from typing_extensions import Self

from galactic.algebras.closure import AbstractClosed, AbstractClosedEnumerable
from galactic.algebras.lattice import (
    AbstractFiniteLattice,
    Element,
    ExtensibleFiniteLattice,
    FrozenFiniteLattice,
)
from galactic.algebras.set import FIFOSet, FiniteSubSet
from galactic.helpers.core import default_repr

from ._attribute import Attribute
from ._connection import Extent, GaloisConnection, Intent
from ._context import Context


class AbstractConcept(Element, Protocol):  # type: ignore[misc]
    """
    A concept has two properties, its extent and its intent.
    """

    @property
    @abstractmethod
    def extent(self) -> AbstractClosedEnumerable[str]:
        """
        Get the concept extent.

        Returns
        -------
        AbstractClosedEnumerable[str]
            The concept extent.

        Raises
        ------
        NotImplementedError
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def intent(self) -> AbstractClosed[Attribute]:
        """
        Get the concept intent.

        Returns
        -------
        Extent
            The concept extent.

        Raises
        ------
        NotImplementedError
        """
        raise NotImplementedError


class AbstractEnumerableConcept(AbstractConcept, Protocol):
    """
    An enumerable concept has two properties, its extent and its intent.

    The intent is an enumerable closed.
    """

    @property
    @abstractmethod
    def intent(self) -> AbstractClosedEnumerable[Attribute]:
        """
        Get the concept intent.

        Returns
        -------
        AbstractClosedEnumerable[Attribute]
            The concept extent.

        Raises
        ------
        NotImplementedError
        """
        raise NotImplementedError


# pylint: disable=protected-access
class Concept(Element):  # type: ignore[misc]
    # noinspection PyUnresolvedReferences,PyTypeChecker
    """
    It represents classical concepts in Formal Concept Analysis.

    Parameters
    ----------
    connection
        The antitone Galois connection.
    objects
        An iterable of object identifiers.
    attributes :
        An iterable of attributes.

    Examples
    --------
    >>> from pprint import pprint
    >>> from galactic.algebras.concept import GaloisConnection, Concept
    >>> from galactic.algebras.examples.concept import context
    >>> attributes = list(context.co_domain)
    >>> connection = GaloisConnection(context)
    >>> top = Concept(connection)
    >>> top
    <galactic.algebras.concept.Concept object at 0x...>
    >>> pprint(list(top.extent))
    ['Dove',
     'Hen',
     'Duck',
     'Goose',
     'Owl',
     'Hawk',
     'Eagle',
     'Fox',
     'Dog',
     'Wolf',
     'Cat',
     'Tiger',
     'Lion',
     'Horse',
     'Zebra',
     'Cow']
    >>> pprint(list(str(attribute) for attribut in top.intent))
    []
    >>> concept1 = Concept(connection, objects=["Dove", "Hen"])
    >>> concept1
    <galactic.algebras.concept.Concept object at 0x...>
    >>> concept2 = Concept(
    ...     connection,
    ...     attributes=[
    ...         attribute for attribute in attributes if str(attribute) == "hunt"
    ...     ],
    ... )
    >>> concept2
    <galactic.algebras.concept.Concept object at 0x...>
    >>> concept = concept1 & concept2
    >>> pprint(list(concept.extent))
    ['Owl', 'Hawk']
    >>> pprint(list(str(attribute) for attribute in concept.intent))
    ['small', 'twolegs', 'feathers', 'fly', 'hunt']
    >>> concept1 = Concept(connection, objects=["Dove"])
    >>> concept2 = Concept(connection, objects=["Hen"])
    >>> concept = concept1 | concept2
    >>> pprint(list(concept.extent))
    ['Dove', 'Hen', 'Duck', 'Goose', 'Owl', 'Hawk']
    >>> pprint(list(str(attribute) for attribute in concept.intent))
    ['small', 'twolegs', 'feathers']

    Examples
    --------
    >>> from pprint import pprint
    >>> from galactic.algebras.concept import GaloisConnection
    >>> from galactic.algebras.examples.concept import context
    >>> from galactic.algebras.concept import Concept, ExtensibleConceptLattice
    >>> connection = GaloisConnection(context)
    >>> lattice = ExtensibleConceptLattice(
    ...     elements=[
    ...         Concept(connection, objects=["Owl", "Cat"]),
    ...         Concept(connection, objects=["Fox"]),
    ...         Concept(connection, objects=["Tiger"]),
    ...     ]
    ... )
    >>> # a lattice is traversed using decreasing support
    >>> pprint([list(concept.extent) for concept in lattice])
    [['Owl', 'Hawk', 'Eagle', 'Fox', 'Wolf', 'Cat', 'Tiger', 'Lion'],
     ['Fox', 'Wolf', 'Cat', 'Tiger', 'Lion'],
     ['Owl', 'Hawk', 'Cat'],
     ['Fox', 'Wolf'],
     ['Tiger', 'Lion'],
     ['Cat'],
     []]
    >>> pprint(
    ...     [
    ...         [str(attribute) for attribute in concept.intent]
    ...         for concept in lattice
    ...     ]
    ... )
    [['hunt'],
     ['hunt', 'fourlegs', 'hair', 'run'],
     ['small', 'hunt'],
     ['hunt', 'medium', 'fourlegs', 'hair', 'run'],
     ['hunt', 'fourlegs', 'hair', 'run', 'big'],
     ['small', 'hunt', 'fourlegs', 'hair', 'run'],
     ['small',
      'twolegs',
      'feathers',
      'fly',
      'swim',
      'hunt',
      'medium',
      'fourlegs',
      'hair',
      'run',
      'mane',
      'big',
      'hooves']]
    >>> # in reverse mode, a lattice is traversed using decreasing intent cardinality
    >>> pprint(
    ...     [
    ...         [str(attribute) for attribute in concept.intent]
    ...         for concept in reversed(lattice)
    ...     ]
    ... )
    [['small',
      'twolegs',
      'feathers',
      'fly',
      'swim',
      'hunt',
      'medium',
      'fourlegs',
      'hair',
      'run',
      'mane',
      'big',
      'hooves'],
     ['small', 'hunt', 'fourlegs', 'hair', 'run'],
     ['hunt', 'medium', 'fourlegs', 'hair', 'run'],
     ['hunt', 'fourlegs', 'hair', 'run', 'big'],
     ['hunt', 'fourlegs', 'hair', 'run'],
     ['small', 'hunt'],
     ['hunt']]
    >>> pprint([list(concept.extent) for concept in reversed(lattice)])
    [[],
     ['Cat'],
     ['Fox', 'Wolf'],
     ['Tiger', 'Lion'],
     ['Fox', 'Wolf', 'Cat', 'Tiger', 'Lion'],
     ['Owl', 'Hawk', 'Cat'],
     ['Owl', 'Hawk', 'Eagle', 'Fox', 'Wolf', 'Cat', 'Tiger', 'Lion']]
    """

    __slots__ = (
        "_hash",
        "_connection",
        "_extent",
        "_intent",
    )

    _hash: int | None
    _connection: GaloisConnection
    _extent: Extent | None
    _intent: Intent | None

    def __init__(
        self,
        connection: GaloisConnection,
        objects: Iterable[str] | None = None,
        attributes: Iterable[Attribute] | None = None,
    ) -> None:
        self._connection = connection
        self._hash: int | None = None
        if objects is None:
            if attributes is None:
                self._extent = self._connection.polarities[1]()
            else:
                self._extent = self._connection.polarities[1](elements=attributes)
            self._intent = None
        else:
            self._intent = self._connection.polarities[0](elements=objects)
            self._extent = None

    def __hash__(self) -> int:
        if self._hash is None:
            self._hash = hash((self._connection, self.extent, self.intent))
        return self._hash

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__) and self._connection is other._connection:
            if self._extent is None:
                return self.intent == other.intent
            return self.extent == other.extent
        return NotImplemented

    def __lt__(self, other: object) -> bool:
        if isinstance(other, self.__class__) and self._connection is other._connection:
            if self._extent is None:
                return self.intent > other.intent  # type: ignore[no-any-return]
            return self.extent < other.extent  # type: ignore[no-any-return]
        return NotImplemented

    def __le__(self, other: object) -> bool:
        if isinstance(other, self.__class__) and self._connection is other._connection:
            if self._extent is None:
                return self.intent >= other.intent  # type: ignore[no-any-return]
            return self.extent <= other.extent  # type: ignore[no-any-return]
        return NotImplemented

    def __gt__(self, other: object) -> bool:
        if isinstance(other, self.__class__) and self._connection is other._connection:
            if self._extent is None:
                return self.intent < other.intent  # type: ignore[no-any-return]
            return self.extent > other.extent  # type: ignore[no-any-return]
        return NotImplemented

    def __ge__(self, other: object) -> bool:
        if isinstance(other, self.__class__) and self._connection is other._connection:
            if self._extent is None:
                return self.intent <= other.intent  # type: ignore[no-any-return]
            return self.extent >= other.extent  # type: ignore[no-any-return]
        return NotImplemented

    def __and__(self, other: object) -> Self:
        if isinstance(other, self.__class__):
            return self.meet(other)
        return NotImplemented

    def __or__(self, other: object) -> Self:
        if isinstance(other, self.__class__):
            return self.join(other)
        return NotImplemented

    def meet(self, *others: Self) -> Self:
        """
        Compute the meet between this concept and others.

        Parameters
        ----------
        *others
            Other concepts

        Returns
        -------
        Self
            The meet between this concept and others.

        """
        for other in others:
            if other.connection is not self.connection:
                raise TypeError
        instance = copy.copy(self)
        instance._hash = None
        instance._extent = copy.copy(self.extent)
        instance._extent._hash = None
        instance._extent._elements = self.extent.elements.intersection(
            *(other.extent.elements for other in others),
        )
        instance._intent = None
        return instance

    def join(self, *others: Self) -> Self:
        """
        Compute the join between this concept and others.

        Parameters
        ----------
        *others
            Other concepts

        Returns
        -------
        Self
            The join between this concept and others.

        """
        for other in others:
            if other.connection is not self.connection:
                raise TypeError
        instance = copy.copy(self)
        instance._hash = None
        instance._intent = copy.copy(self.intent)
        instance._intent._hash = None
        instance._intent._elements = self.intent.elements.intersection(
            *(other.intent.elements for other in others),
        )
        instance._extent = None
        return instance

    @property
    def connection(self) -> GaloisConnection:
        """
        Get the antitone Galois connection.

        Returns
        -------
        GaloisConnection
            The antitone Galois connection.

        """
        return self._connection

    @property
    def context(self) -> Context:
        """
        Get the context of the antitone Galois connection.

        Returns
        -------
        Context
            The context of the antitone Galois connection.

        """
        return self._connection.context

    @property
    def extent(self) -> Extent:
        """
        Get the concept extent.

        Returns
        -------
        Extent
            The concept extent.

        """
        if self._extent is None:
            self._extent = self._connection.polarities[1](self.intent)
        return self._extent

    @property
    def intent(self) -> Intent:
        """
        Get the concept intent.

        Returns
        -------
        Intent
            The concept intent.

        """
        if self._intent is None:
            self._intent = self._connection.polarities[0](self.extent)
        return self._intent

    def meet_priority(self, generators: Iterable[Self]) -> int:
        """
        Compute the priority of an element considering an iterable of generators.

        This methods returns the negative value of the support.
        This ensures a traversal of the lattice in decreasing order of support.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The priority.

        """
        return -len(self.extent)

    def join_priority(self, generators: Iterable[Self]) -> int:
        """
        Compute the priority of an element considering an iterable of generators.

        This methods returns the negative value of the cardinality of the intent.
        This ensures a reverse traversal of the lattice in decreasing order of
        the cardinality of the intent.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The priority.

        """
        return -len(self.intent)


class Objects(Collection[str]):
    """
    It represents sets of object identifiers.

    Parameters
    ----------
    connection
        The underlying antitone Galois connection.
    elements
        The object identifiers.

    """

    __slots__ = ("_elements",)

    def __init__(self, connection: GaloisConnection, elements: Iterable[str]) -> None:
        self._elements = FiniteSubSet[str](
            connection.context.universes[0],
            elements,
        )

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __contains__(self, value: object) -> bool:
        return value in self._elements

    def __len__(self) -> int:
        return len(self._elements)

    def __iter__(self) -> Iterator[str]:
        return iter(self._elements)


class Attributes(Collection[Attribute]):
    """
    It represents sets of attributes.

    Parameters
    ----------
    connection
        The underlying antitone Galois connection.
    elements :
        The attributes.

    """

    __slots__ = ("_elements",)

    def __init__(
        self,
        connection: GaloisConnection,
        elements: Iterable[Attribute],
    ) -> None:
        self._elements = FiniteSubSet[Attribute](
            connection.context.universes[1],
            elements,
        )

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __contains__(self, value: object) -> bool:
        return value in self._elements

    def __len__(self) -> int:
        return len(self._elements)

    def __iter__(self) -> Iterator[Attribute]:
        return iter(self._elements)


class ObjectConcepts(Mapping[AbstractConcept, Objects]):
    """
    It represents a mapping from concept to object identifiers.

    Parameters
    ----------
    concepts
        A mapping from concept to object identifiers.

    """

    __slots__ = ("_concepts",)

    def __init__(self, concepts: dict[AbstractConcept, Objects]) -> None:
        self._concepts = concepts

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __getitem__(self, concept: AbstractConcept) -> Objects:
        return self._concepts[concept]

    def __len__(self) -> int:
        return len(self._concepts)

    def __iter__(self) -> Iterator[AbstractConcept]:
        return iter(self._concepts)


class AttributeConcepts(Mapping[AbstractEnumerableConcept, Attributes]):
    """
    It represents a mapping from concept to attributes.

    Parameters
    ----------
    concepts
        A mapping from concept to attributes.

    """

    __slots__ = ("_concepts",)

    def __init__(self, concepts: dict[AbstractEnumerableConcept, Attributes]) -> None:
        self._concepts = concepts

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __getitem__(self, concept: AbstractEnumerableConcept) -> Attributes:
        return self._concepts[concept]

    def __len__(self) -> int:
        return len(self._concepts)

    def __iter__(self) -> Iterator[AbstractEnumerableConcept]:
        return iter(self._concepts)


class AbstractConceptLattice(
    AbstractFiniteLattice[AbstractEnumerableConcept],  # type: ignore[misc]
    Collection[AbstractEnumerableConcept],
    Protocol,
):
    """
    It defines the abstract class for concept lattices.
    """

    @property
    def object_concepts(self) -> ObjectConcepts:
        """
        Get the object concepts mapping.

        Returns
        -------
        ObjectConcepts
            The object concepts mapping.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @property
    def attribute_concepts(self) -> AttributeConcepts:
        """
        Get the attribute concepts mapping.

        Returns
        -------
        AttributeConcepts
            The attribute concepts mapping.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


# pylint: disable=too-many-ancestors
class FrozenConceptLattice(FrozenFiniteLattice[Concept]):  # type: ignore[misc]
    """
    It represents frozen concept lattices.

    Parameters
    ----------
    *others
        Sequence of ConceptLattice using the same antitone Galois connection
    elements
        Iterable of Concept using the same antitone Galois connection

    """

    __slots__ = (
        "_object_concept_mapping",
        "_attribute_concept_mapping",
        "_object_concepts",
        "_attribute_concepts",
        "_object_family",  # @chdemko: fix Moore families
        "_attribute_family",
    )

    def __init__(
        self,
        *others: AbstractConceptLattice,
        elements: Iterable[Concept] | None = None,
    ) -> None:
        self._object_concept_mapping: dict[Concept, Objects] = {}
        self._attribute_concept_mapping: dict[Concept, Attributes] = {}
        self._object_concepts = ObjectConcepts(self._object_concept_mapping)
        self._attribute_concepts = AttributeConcepts(self._attribute_concept_mapping)
        super().__init__(*others, elements=elements)

    # pylint: disable=too-many-branches, too-many-locals
    def _reduced_context_completion(self, iterable: Iterable[Concept]) -> None:
        """
        Optimised version of the reduced context completion algorithm.

        Parameters
        ----------
        iterable
            An iterable of concepts.

        """
        # Select only new concepts
        concepts = [concept for concept in iterable if concept not in self]
        if not concepts:
            return

        # Update minimum and maximum
        if self.minimum is None:
            minimum = concepts[0].meet(*concepts[1:])
            self._set_minimum(minimum)
            maximum = concepts[0].join(*concepts[1:])
            if minimum != maximum:
                self._set_maximum(maximum)
            else:
                self._set_maximum(minimum)
        else:
            # Keep existing minimum/maximum
            concepts.append(self.minimum)
            if self.minimum is not self.maximum:
                concepts.append(self.maximum)
            self._set_minimum(self.minimum.meet(*concepts))
            self._set_maximum(self.maximum.join(*concepts))

        # First iteration, compute concept for each singleton
        object_concept: dict[str, Concept] = {
            key: self.maximum.meet(
                *(
                    concept
                    for concept in itertools.chain(self.meet_irreducible, concepts)
                    if key in concept.extent
                ),
            )
            for key in self.maximum.extent
        }
        attribute_concept: dict[Attribute, Concept] = {
            attribute: self.minimum.join(
                *(
                    concept
                    for concept in itertools.chain(self.join_irreducible, concepts)
                    if attribute in concept.intent
                ),
            )
            for attribute in self.minimum.intent
        }

        # Loop on evolution
        cont = True
        while cont:
            cont = False
            for key, value in object_concept.items():
                concept = self.maximum.meet(
                    *(
                        concept
                        for concept in attribute_concept.values()
                        if key in concept.extent
                    ),
                )
                if concept != value:
                    cont = True
                    object_concept[key] = concept
            for attribute, value in attribute_concept.items():
                concept = self.minimum.join(
                    *(
                        concept
                        for concept in object_concept.values()
                        if attribute in concept.intent
                    ),
                )
                if concept != value:
                    cont = True
                    attribute_concept[attribute] = concept

        # Update attribute_concept_mapping and object_concept_mapping
        object_concept_mapping: dict[Concept, FIFOSet[str]] = defaultdict(
            FIFOSet[str],
        )
        attribute_concept_mapping: dict[Concept, FIFOSet[Attribute]] = defaultdict(
            FIFOSet[Attribute],
        )
        self._object_concept_mapping.clear()
        self._attribute_concept_mapping.clear()

        for key, concept in object_concept.items():
            object_concept_mapping[concept].add(key)

        for attribute, concept in attribute_concept.items():
            attribute_concept_mapping[concept].add(attribute)

        for concept, objects in object_concept_mapping.items():
            self._object_concept_mapping[concept] = Objects(
                self.maximum.connection,
                objects,
            )
        for concept, attributes in attribute_concept_mapping.items():
            self._attribute_concept_mapping[concept] = Attributes(
                self.maximum.connection,
                attributes,
            )

        # Update irreducible
        self._set_meet_irreducible(attribute_concept_mapping)
        self._set_join_irreducible(object_concept_mapping)

    @property
    def connection(self) -> GaloisConnection | None:
        """
        Get the underlying antitone Galois connection.

        Returns
        -------
        GaloisConnection | None
            The antitone Galois connection.
        """
        if self.maximum:
            return self.maximum.connection  # type: ignore[no-any-return]
        return None

    @property
    def context(self) -> Context | None:
        """
        Get the underlying context of the antitone Galois connection.

        Returns
        -------
        Context | None
            The context of the antitone Galois connection.
        """
        if self.maximum:
            return self.maximum.connection.context  # type: ignore[no-any-return]
        return None

    @property
    def attribute_concepts(self) -> AttributeConcepts:
        """
        Get the attribute concepts mapping.

        Returns
        -------
        AttributeConcepts
            The attribute concepts mapping.

        """
        return self._attribute_concepts

    @property
    def object_concepts(self) -> ObjectConcepts:
        """
        Get the object concepts mapping.

        Returns
        -------
        ObjectConcepts
            The object concepts mapping.

        """
        return self._object_concepts


class ExtensibleConceptLattice(ExtensibleFiniteLattice[Concept], FrozenConceptLattice):  # type: ignore[misc]
    """
    It represents extensible concept lattices.

    Examples
    --------
    >>> from pprint import pprint
    >>> from galactic.algebras.concept import GaloisConnection, Concept
    >>> from galactic.algebras.examples.concept import context
    >>> co_domain = list(context.co_domain)
    >>> connection = GaloisConnection(context)
    >>> lattice1 = ExtensibleConceptLattice()
    >>> lattice1.extend(
    ...     [
    ...         Concept(connection, objects=["Dove"]),
    ...         Concept(connection, objects=["Hen"]),
    ...     ],
    ... )
    >>> # ConceptLattice acts as a closure operator!
    >>> lattice2 = ExtensibleConceptLattice(
    ...     elements=[
    ...         Concept(connection, attributes=[co_domain[0]]),
    ...         Concept(connection, attributes=[co_domain[1]]),
    ...     ],
    ... )
    >>> lattice = ExtensibleConceptLattice(lattice1, lattice2)
    >>> lattice.connection
    <galactic.algebras.concept.GaloisConnection object at 0x...>
    >>> lattice.connection is lattice1.connection is lattice2.connection
    True
    >>> pprint(
    ...     [
    ...         list(objects)
    ...         for concept, objects in lattice.object_concepts.items()
    ...     ]
    ... )
    [['Dove', 'Duck', 'Goose', 'Owl', 'Hawk'],
     ['Hen'],
     ['Eagle'],
     ['Fox', 'Dog', 'Wolf', 'Tiger', 'Lion', 'Horse', 'Zebra', 'Cow'],
     ['Cat']]
    >>> pprint(
    ...     [
    ...         list(attributes)
    ...         for concept, attributes in lattice.attribute_concepts.items()
    ...     ]
    ... )
    [[<function small at 0x...>],
     [<function twolegs at 0x...>, <function feathers at 0x...>]]
    """

    __slots__ = ()
