r"""
The :mod:`galactic.algebras.concept` module.

It defines several classes and functions:

..  rubric:: Attributes

* :func:`member` for creating an attribute using a membership test;
* :func:`relation` for creating an attribute using a relation test;
* :class:`Attribute` for defining attributes.

..  rubric:: Contexts

* :class:`AbstractContext` for representing abstract context;
* :class:`Context` for representing enumerable context;
* :func:`create_context_from_data` for creating a context from data;
* :func:`create_context_from_table` for creating a context from tables.

..  rubric:: Closed sets and Galois connections

* :class:`Extent` for representing closed sets of object identifiers;
* :class:`Intent` for representing closed sets of attributes;
* :class:`AttributePolarity` for representing attribute polarity
  (the :math:`\alpha` function);
* :class:`ObjectPolarity` for representing object polarity
  (the :math:`\beta` function);
* :class:`GaloisConnection` for representing Galois connections.

..  rubric:: Concepts and concept lattices

* :class:`AbstractConcept` for representing an abstract concept;
* :class:`AbstractEnumerableConcept` for representing an abstract enumerable concept;
* :class:`Concept` for representing a concrete enumerable concept;
* :class:`AbstractConceptLattice` for representing abstract enumerable
  concepts lattices;
* :class:`FrozenConceptLattice` for representing frozen enumerable concepts lattices;
* :class:`ExtensibleConceptLattice` for representing extensible enumerable concepts
  lattices;
* :class:`ObjectConcepts` and :class:`Objects` for representing object concepts;
* :class:`AttributeConcepts` and :class:`Attributes` for representing attribute
  concepts.

..  rubric:: Moore families of extents and intents

* :class:`ExtentLattice` and :class:`ObjectExtents` for representing extent lattices
  (Moore family of extents) which acts as a view on an concept lattice;
* :class:`IntentLattice` and :class:`AttributeIntents` for representing intent lattices
  (Moore family of intents) which acts as a view on an concept lattice.
"""

from ._attribute import (
    Attribute,
    member,
    relation,
)
from ._concept import (
    AbstractConcept,
    AbstractConceptLattice,
    AbstractEnumerableConcept,
    AttributeConcepts,
    Attributes,
    Concept,
    ExtensibleConceptLattice,
    FrozenConceptLattice,
    ObjectConcepts,
    Objects,
)
from ._connection import (
    AttributePolarity,
    Extent,
    GaloisConnection,
    Intent,
    ObjectPolarity,
)
from ._context import (
    AbstractContext,
    Context,
    create_context_from_data,
    create_context_from_table,
)
from ._family import (
    AttributeIntents,
    ExtentLattice,
    IntentLattice,
    ObjectExtents,
)

__all__ = (
    "Attribute",
    "member",
    "relation",
    "AbstractContext",
    "Context",
    "create_context_from_data",
    "create_context_from_table",
    "Extent",
    "Intent",
    "ObjectPolarity",
    "AttributePolarity",
    "GaloisConnection",
    "AbstractConcept",
    "AbstractEnumerableConcept",
    "Concept",
    "AbstractConceptLattice",
    "AbstractConceptLattice",
    "ExtensibleConceptLattice",
    "FrozenConceptLattice",
    "AttributeConcepts",
    "ObjectConcepts",
    "Attributes",
    "Objects",
    "ExtentLattice",
    "ObjectExtents",
    "IntentLattice",
    "AttributeIntents",
)
