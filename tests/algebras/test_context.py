"""Test context module."""

from unittest import TestCase

from galactic.algebras.concept import Context, create_context_from_table
from galactic.algebras.examples.concept import animals, context
from galactic.helpers.population import Population


class ContextTestCase(TestCase):
    def test___init__(self):
        self.assertEqual(
            list(context.domain),
            list(animals.keys()),
        )

    def test_empty(self):
        population = Population()
        empty = Context(population, [])
        self.assertEqual(list(empty.domain), [])
        self.assertEqual(list(empty.co_domain), [])
        self.assertEqual(list(empty), [])

    def test_successors(self):
        successors = context.successors("Dove")
        self.assertEqual(
            [str(attribute) for attribute in successors],
            ["small", "twolegs", "feathers", "fly"],
        )
        self.assertIn(context.co_domain[0], successors)
        self.assertEqual(len(successors), 4)
        with self.assertRaises(ValueError):
            _ = context.successors("unknown")

    def test_predecessors(self):
        attribute = context.co_domain[0]
        predecessors = context.predecessors(attribute)
        self.assertEqual(
            list(predecessors),
            ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Cat"],
        )
        self.assertIn("Dove", predecessors)
        self.assertNotIn(1, predecessors)
        self.assertEqual(len(predecessors), 7)
        with self.assertRaises(ValueError):
            _ = context.predecessors(bool)

    def test___contains__(self):
        self.assertIn(
            ("Dove", context.co_domain[0]),
            context,
        )
        self.assertNotIn(
            ("Dove", bool),
            context,
        )
        self.assertNotIn(
            ("Dove", 1),
            context,
        )
        self.assertNotIn(
            4,
            context,
        )

    def test___len__(self):
        self.assertEqual(len(context), sum(len(value) for value in animals.values()))

    def test_create_context_from_table(self):
        attributes = ["composite", "even", "square", "odd", "prime"]
        objects = {
            "0": [True, True, True, False, False],
            "1": [False, False, True, True, False],
            "2": [False, True, False, False, True],
            "3": [False, False, False, True, True],
            "4": [True, True, True, False, False],
            "5": [False, False, False, True, True],
            "6": [True, True, False, False, False],
            "7": [False, False, False, True, True],
            "8": [True, True, False, False, False],
            "9": [True, False, True, True, False],
        }
        context_bis = create_context_from_table(objects, attributes)
        self.assertEqual(
            [(key, str(attribute)) for key, attribute in context_bis],
            [
                ("0", "composite"),
                ("0", "even"),
                ("0", "square"),
                ("1", "square"),
                ("1", "odd"),
                ("2", "even"),
                ("2", "prime"),
                ("3", "odd"),
                ("3", "prime"),
                ("4", "composite"),
                ("4", "even"),
                ("4", "square"),
                ("5", "odd"),
                ("5", "prime"),
                ("6", "composite"),
                ("6", "even"),
                ("7", "odd"),
                ("7", "prime"),
                ("8", "composite"),
                ("8", "even"),
                ("9", "composite"),
                ("9", "square"),
                ("9", "odd"),
            ],
        )
        attributes = ["composite", "even", "square", "odd", "prime"]
        objects = [
            [True, True, True, False, False],
            [False, False, True, True, False],
            [False, True, False, False, True],
            [False, False, False, True, True],
            [True, True, True, False, False],
            [False, False, False, True, True],
            [True, True, False, False, False],
            [False, False, False, True, True],
            [True, True, False, False, False],
            [True, False, True, True, False],
        ]
        context_bis = create_context_from_table(objects, attributes)
        self.assertEqual(
            [(key, str(attribute)) for key, attribute in context_bis],
            [
                ("0", "composite"),
                ("0", "even"),
                ("0", "square"),
                ("1", "square"),
                ("1", "odd"),
                ("2", "even"),
                ("2", "prime"),
                ("3", "odd"),
                ("3", "prime"),
                ("4", "composite"),
                ("4", "even"),
                ("4", "square"),
                ("5", "odd"),
                ("5", "prime"),
                ("6", "composite"),
                ("6", "even"),
                ("7", "odd"),
                ("7", "prime"),
                ("8", "composite"),
                ("8", "even"),
                ("9", "composite"),
                ("9", "square"),
                ("9", "odd"),
            ],
        )
