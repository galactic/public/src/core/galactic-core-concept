"""Connection test module."""

from unittest import TestCase

from galactic.algebras.concept import GaloisConnection
from galactic.algebras.examples.concept import context

attributes = list(context.co_domain)


class ConnectionTestCase(TestCase):
    def test___init__(self):
        connection = GaloisConnection(context)
        self.assertIs(context, connection.context)

    def test_attribute_polarity(self):
        connection = GaloisConnection(context)
        self.assertEqual(
            [
                str(attribute)
                for attribute in connection.polarities[0](
                    elements=["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                )
            ],
            ["twolegs", "feathers"],
        )

    def test_object_polarity(self):
        connection = GaloisConnection(context)
        self.assertEqual(
            list(connection.polarities[1](elements=[attributes[0], attributes[1]])),
            ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
        )

    def test_attribute_closure(self):
        connection = GaloisConnection(context)
        self.assertEqual(
            [
                str(attribute)
                for attribute in connection.closures[1](
                    elements=[attributes[0], attributes[1]],
                )
            ],
            ["small", "twolegs", "feathers"],
        )

    def test_object_closure(self):
        connection = GaloisConnection(context)
        self.assertEqual(
            list(connection.closures[0](elements=["Dove", "Hen"])),
            ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
        )
