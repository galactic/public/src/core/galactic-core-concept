"""Attribute test module."""

from unittest import TestCase

from galactic.algebras.concept import member, relation


class AttributeTestCase(TestCase):
    def test_member(self):
        odd = member("odd")
        self.assertTrue(odd(["odd", "prime"]))
        self.assertFalse(odd(["prime"]))
        self.assertEqual(str(odd), "odd")

    def test_relation(self):
        odd = relation("odd", 1)
        self.assertTrue(odd([False, True]))
        self.assertFalse(odd([False, False]))
        self.assertEqual(str(odd), "odd")
