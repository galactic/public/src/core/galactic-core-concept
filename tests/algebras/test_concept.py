"""Test concept module."""

import copy
from unittest import TestCase

from galactic.algebras.concept import (
    Concept,
    Context,
    ExtensibleConceptLattice,
    GaloisConnection,
    create_context_from_data,
)
from galactic.algebras.examples.concept import context
from galactic.helpers.population import Population


class ConceptTestCase(TestCase):
    def test___init__(self):
        connection = GaloisConnection(context)
        concept = Concept(connection)
        self.assertIs(concept.connection, connection)

    def test_empty(self):
        population = Population()
        empty = Context(population, [])
        connection = GaloisConnection(empty)
        concept = Concept(connection)
        self.assertEqual(list(concept.intent), [])
        self.assertEqual(list(concept.extent), [])
        lattice = ExtensibleConceptLattice(elements=[concept])
        self.assertIs(lattice.minimum, lattice.maximum)

    def test_full(self):
        connection = GaloisConnection(create_context_from_data({"0": ["a"]}))
        lattice = ExtensibleConceptLattice(
            elements=[Concept(connection, objects=["0"])],
        )
        self.assertIs(lattice.minimum, lattice.maximum)

    def test_extent(self):
        connection = GaloisConnection(context)
        concept = Concept(connection)
        self.assertEqual(
            list(concept.extent),
            [
                "Dove",
                "Hen",
                "Duck",
                "Goose",
                "Owl",
                "Hawk",
                "Eagle",
                "Fox",
                "Dog",
                "Wolf",
                "Cat",
                "Tiger",
                "Lion",
                "Horse",
                "Zebra",
                "Cow",
            ],
        )
        concept = Concept(connection, objects=["Dove", "Hen"])
        self.assertEqual(
            list(concept.extent),
            ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
        )
        concept = Concept(
            connection=connection,
            attributes=[context.attribute("small"), context.attribute("twolegs")],
        )
        self.assertEqual(
            list(concept.extent),
            ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
        )
        concept = Concept(connection, attributes=context.co_domain)
        self.assertEqual(
            list(concept.extent),
            [],
        )

    def test_intent(self):
        connection = GaloisConnection(context)
        concept = Concept(connection)
        self.assertEqual(
            [str(attribute) for attribute in concept.intent],
            [],
        )
        concept = Concept(connection, objects=["Dove", "Hen"])
        self.assertEqual(
            [str(attribute) for attribute in concept.intent],
            ["small", "twolegs", "feathers"],
        )
        concept = Concept(
            connection=connection,
            attributes=[context.attribute("small"), context.attribute("twolegs")],
        )
        self.assertEqual(
            [str(attribute) for attribute in concept.intent],
            ["small", "twolegs", "feathers"],
        )
        concept = Concept(connection, objects=[])
        self.assertEqual(
            [str(attribute) for attribute in concept.intent],
            [
                "small",
                "twolegs",
                "feathers",
                "fly",
                "swim",
                "hunt",
                "medium",
                "fourlegs",
                "hair",
                "run",
                "mane",
                "big",
                "hooves",
            ],
        )

    def test___eq__(self):
        connection = GaloisConnection(context)
        concept = Concept(connection)
        self.assertEqual(concept, Concept(connection))
        self.assertNotEqual(concept, Concept(GaloisConnection(context)))
        self.assertNotEqual(concept, Concept(connection, objects=[]))

    def test___lt__(self):
        connection = GaloisConnection(context)
        concept = Concept(connection)
        self.assertLess(Concept(connection, objects=[]), concept)
        self.assertFalse(concept < Concept(connection))
        self.assertFalse(concept < Concept(connection, objects=[]))

    def test___gt__(self):
        connection = GaloisConnection(context)
        concept = Concept(connection)
        self.assertGreater(concept, Concept(connection, objects=[]))
        self.assertFalse(Concept(connection) > concept)
        self.assertFalse(Concept(connection, objects=[]) > concept)

    def test___and__(self):
        connection = GaloisConnection(context)
        concept1 = Concept(connection, objects=["Dove", "Hen"])
        concept2 = Concept(
            connection,
            attributes=[context.attribute("hunt")],
        )
        concept = concept1 & concept2
        self.assertEqual(
            list(concept.extent),
            ["Owl", "Hawk"],
        )
        self.assertEqual(
            [str(attribute) for attribute in concept.intent],
            ["small", "twolegs", "feathers", "fly", "hunt"],
        )

    def test___or__(self):
        connection = GaloisConnection(context)
        concept1 = Concept(connection, objects=["Dove"])
        concept2 = Concept(connection, objects=["Hen"])
        concept = concept1 | concept2
        self.assertEqual(
            list(concept.extent),
            ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
        )
        self.assertEqual(
            [str(attribute) for attribute in concept.intent],
            ["small", "twolegs", "feathers"],
        )

    def test___copy__(self):
        connection = GaloisConnection(context)
        concept = Concept(connection, objects=["Dove"])
        clone = copy.copy(concept)
        self.assertEqual(concept, clone)
        self.assertIs(concept.extent, concept.extent)
