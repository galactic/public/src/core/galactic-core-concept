"""Test lattice module."""

from unittest import TestCase

from galactic.algebras.concept import (
    Concept,
    ExtensibleConceptLattice,
    Extent,
    ExtentLattice,
    GaloisConnection,
    Intent,
    IntentLattice,
)
from galactic.algebras.examples.concept import context


class ExtensibleConceptLatticeTestCase(TestCase):
    def test___init__(self):
        connection = GaloisConnection(context)
        lattice1 = ExtensibleConceptLattice()
        lattice1.extend(
            [
                Concept(connection, objects=["Dove"]),
                Concept(connection, objects=["Hen"]),
            ],
        )
        co_domain = list(context.co_domain)
        lattice2 = ExtensibleConceptLattice(
            elements=[
                Concept(connection, attributes=[co_domain[0]]),
                Concept(connection, attributes=[co_domain[1]]),
            ],
        )
        lattice = ExtensibleConceptLattice(lattice1, lattice2)
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                ["Hen"],
                ["Eagle"],
                ["Fox", "Dog", "Wolf", "Tiger", "Lion", "Horse", "Zebra", "Cow"],
                ["Cat"],
            ],
        )
        dummy = GaloisConnection(context)
        with self.assertRaises(TypeError):
            lattice.extend([Concept(dummy)])

    def test__reduced_context_completion_from_objects_1(self):
        connection = GaloisConnection(context)
        lattice = ExtensibleConceptLattice()
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [],
        )

        lattice.extend([Concept(connection, objects=["Dove"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [["Dove", "Duck", "Goose", "Owl", "Hawk"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [],
        )

        lattice.extend([Concept(connection, objects=["Hen"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [["Dove", "Duck", "Goose", "Owl", "Hawk"], ["Hen"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [["Dove", "Duck", "Goose", "Owl", "Hawk"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"]],
        )

        lattice.extend([Concept(connection, objects=["Duck"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [["Dove", "Owl", "Hawk"], ["Hen"], ["Duck", "Goose"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [["Duck", "Goose"], ["Dove", "Duck", "Goose", "Owl", "Hawk"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Goose"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [["Dove", "Owl", "Hawk"], ["Hen"], ["Duck", "Goose"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [["Duck", "Goose"], ["Dove", "Duck", "Goose", "Owl", "Hawk"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Owl"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [["Dove"], ["Hen"], ["Duck", "Goose"], ["Owl", "Hawk"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Dove", "Duck", "Goose", "Owl", "Hawk"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Eagle"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [["Dove"], ["Hen"], ["Duck", "Goose"], ["Owl", "Hawk"], ["Eagle"]],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk", "Eagle"],
                ["Eagle"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Fox"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Wolf"],
                ["Dog", "Horse", "Zebra", "Cow"],
                ["Cat", "Tiger", "Lion"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Eagle", "Fox", "Wolf"],
                ["Fox", "Wolf"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Wolf"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Dog"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Wolf"],
                ["Dog"],
                ["Cat", "Tiger", "Lion"],
                ["Horse", "Zebra", "Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Fox", "Dog", "Wolf"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ["Eagle", "Fox", "Dog", "Wolf"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Fox", "Wolf"],
                ["Eagle"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Fox", "Dog", "Wolf"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Wolf"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat", "Tiger", "Lion"],
                ["Horse", "Zebra", "Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Fox", "Dog", "Wolf"],
                ["Wolf"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ["Eagle", "Fox", "Dog", "Wolf"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Fox", "Wolf"],
                ["Wolf"],
                ["Eagle"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Fox", "Dog", "Wolf"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Cat"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Wolf"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Cat"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ["Eagle", "Fox", "Dog", "Wolf"],
                ["Fox", "Dog", "Wolf", "Cat", "Tiger", "Lion", "Horse", "Zebra"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Fox", "Wolf"],
                ["Wolf"],
                ["Cat"],
                ["Fox", "Dog", "Wolf"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Wolf"],
                ["Tiger", "Lion"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Cat"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ["Eagle", "Fox", "Dog", "Wolf"],
                ["Fox", "Dog", "Wolf", "Cat", "Tiger", "Lion", "Horse", "Zebra"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Fox", "Wolf"],
                ["Wolf"],
                ["Fox", "Dog", "Wolf"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Lion"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger"],
                ["Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Wolf", "Lion"],
                ["Tiger", "Lion"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Cat"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ["Eagle", "Fox", "Dog", "Wolf"],
                ["Fox", "Dog", "Wolf", "Cat", "Tiger", "Lion", "Horse", "Zebra"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Fox", "Wolf"],
                ["Wolf"],
                ["Lion"],
                ["Fox", "Dog", "Wolf"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Horse"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger"],
                ["Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Wolf", "Lion", "Horse", "Zebra"],
                ["Tiger", "Lion", "Horse", "Zebra"],
                ["Horse", "Zebra"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Cat"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ["Eagle", "Fox", "Dog", "Wolf"],
                ["Fox", "Dog", "Wolf", "Cat", "Tiger", "Lion", "Horse", "Zebra"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Fox", "Wolf"],
                ["Wolf"],
                ["Lion"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Dog", "Wolf"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Zebra"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger"],
                ["Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Wolf", "Lion", "Horse", "Zebra"],
                ["Tiger", "Lion", "Horse", "Zebra"],
                ["Horse", "Zebra"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Cat"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ["Eagle", "Fox", "Dog", "Wolf"],
                ["Fox", "Dog", "Wolf", "Cat", "Tiger", "Lion", "Horse", "Zebra"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Fox", "Wolf"],
                ["Wolf"],
                ["Lion"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Dog", "Wolf"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra"],
            ],
        )

        lattice.extend([Concept(connection, objects=["Cow"])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger"],
                ["Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.meet_irreducible],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Duck", "Goose"],
                ["Fox", "Dog", "Wolf", "Cat", "Tiger", "Lion", "Horse", "Zebra"],
                ["Wolf", "Lion", "Horse", "Zebra"],
                ["Tiger", "Lion", "Horse", "Zebra", "Cow"],
                ["Horse", "Zebra", "Cow"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Cat"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Eagle"],
                ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ["Eagle", "Fox", "Dog", "Wolf"],
                ["Fox", "Dog", "Wolf", "Cat", "Tiger", "Lion", "Horse", "Zebra", "Cow"],
            ],
        )
        self.assertEqual(
            [list(concept.extent) for concept in lattice.join_irreducible],
            [
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Fox", "Wolf"],
                ["Wolf"],
                ["Lion"],
                ["Horse", "Zebra"],
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Dog", "Wolf"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra", "Cow"],
            ],
        )

    def test__reduced_context_completion_from_attributes(self):
        connection = GaloisConnection(context)
        lattice = ExtensibleConceptLattice()
        co_domain = list(context.co_domain)
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[0]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk", "Cat"]],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [["small"]],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[1]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Dog", "Wolf", "Tiger", "Lion", "Horse", "Zebra", "Cow"],
                ["Cat"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [["small"], ["twolegs", "feathers"]],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[2]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Dog", "Wolf", "Tiger", "Lion", "Horse", "Zebra", "Cow"],
                ["Cat"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [["small"], ["twolegs", "feathers"]],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[3]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                ["Hen"],
                ["Eagle"],
                ["Fox", "Dog", "Wolf", "Tiger", "Lion", "Horse", "Zebra", "Cow"],
                ["Cat"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [["small"], ["twolegs", "feathers"], ["fly"]],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[4]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove", "Owl", "Hawk"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Eagle"],
                ["Fox", "Dog", "Wolf", "Tiger", "Lion", "Horse", "Zebra", "Cow"],
                ["Cat"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [["small"], ["twolegs", "feathers"], ["fly"], ["swim"]],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[5]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Wolf", "Tiger", "Lion"],
                ["Dog", "Horse", "Zebra", "Cow"],
                ["Cat"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [
                ["small"],
                ["twolegs", "feathers"],
                ["fly"],
                ["swim"],
                ["hunt"],
                ["medium", "fourlegs", "hair", "run", "mane", "big", "hooves"],
            ],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[6]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Wolf"],
                ["Dog"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra", "Cow"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [
                ["small"],
                ["twolegs", "feathers"],
                ["fly"],
                ["swim"],
                ["hunt"],
                ["medium"],
                ["fourlegs", "hair", "run", "mane", "big", "hooves"],
            ],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[7]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Wolf"],
                ["Dog"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [
                ["small"],
                ["twolegs", "feathers"],
                ["fly"],
                ["swim"],
                ["hunt"],
                ["medium"],
                ["fourlegs", "hair"],
                ["run"],
                ["mane", "big", "hooves"],
            ],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[8]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Wolf"],
                ["Dog"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [
                ["small"],
                ["twolegs", "feathers"],
                ["fly"],
                ["swim"],
                ["hunt"],
                ["medium"],
                ["fourlegs", "hair"],
                ["run"],
                ["mane", "big", "hooves"],
            ],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[9]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox", "Wolf"],
                ["Dog"],
                ["Cat"],
                ["Tiger", "Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [
                ["small"],
                ["twolegs", "feathers"],
                ["fly"],
                ["swim"],
                ["hunt"],
                ["medium"],
                ["fourlegs", "hair"],
                ["run"],
                ["mane", "big", "hooves"],
            ],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[10]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger"],
                ["Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [
                ["small"],
                ["twolegs", "feathers"],
                ["fly"],
                ["swim"],
                ["hunt"],
                ["medium"],
                ["fourlegs", "hair"],
                ["run"],
                ["mane"],
                ["big", "hooves"],
            ],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[11]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger"],
                ["Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [
                ["small"],
                ["twolegs", "feathers"],
                ["fly"],
                ["swim"],
                ["hunt"],
                ["medium"],
                ["fourlegs", "hair"],
                ["run"],
                ["mane"],
                ["big"],
                ["hooves"],
            ],
        )

        lattice.extend([Concept(connection, attributes=[co_domain[12]])])
        self.assertEqual(
            [list(objects) for concept, objects in lattice.object_concepts.items()],
            [
                ["Dove"],
                ["Hen"],
                ["Duck", "Goose"],
                ["Owl", "Hawk"],
                ["Eagle"],
                ["Fox"],
                ["Dog"],
                ["Wolf"],
                ["Cat"],
                ["Tiger"],
                ["Lion"],
                ["Horse", "Zebra"],
                ["Cow"],
            ],
        )
        self.assertEqual(
            [
                [str(attribute) for attribute in attributes]
                for concept, attributes in lattice.attribute_concepts.items()
            ],
            [
                ["small"],
                ["twolegs", "feathers"],
                ["fly"],
                ["swim"],
                ["hunt"],
                ["medium"],
                ["fourlegs", "hair"],
                ["run"],
                ["mane"],
                ["big"],
                ["hooves"],
            ],
        )

    def test__reduced_context_completion_from_objects_2(self):
        connection = GaloisConnection(context)
        lattice_1 = ExtensibleConceptLattice()
        lattice_1.extend(
            [
                Concept(connection, objects=["Owl", "Cat"]),
                Concept(connection, objects=["Fox"]),
                Concept(connection, objects=["Tiger"]),
            ],
        )
        lattice_2 = ExtensibleConceptLattice()
        lattice_2.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice_2.extend([Concept(connection, objects=["Fox"])])
        lattice_2.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(lattice_1, lattice_2)
        self.assertEqual(len(lattice_1.meet_irreducible), 4)
        self.assertEqual(len(lattice_1.join_irreducible), 4)


class ExtentFamilyTestCase(TestCase):
    def test___init__(self):
        connection = GaloisConnection(context)
        lattice = ExtensibleConceptLattice()
        lattice.extend(
            [
                Concept(connection, objects=["Dove"]),
                Concept(connection, objects=["Hen"]),
            ],
        )
        family = ExtentLattice(lattice)
        self.assertEqual(
            [list(extent) for extent in family],
            [
                ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ["Dove", "Duck", "Goose", "Owl", "Hawk"],
            ],
        )
        self.assertEqual(
            [
                (list(extent), list(keys))
                for extent, keys in family.object_extents.items()
            ],
            [
                (
                    ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                    ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                ),
                (["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"], ["Hen"]),
            ],
        )
        self.assertEqual(len(family), 2)

    def test_top(self):
        lattice = ExtensibleConceptLattice()
        family = ExtentLattice(lattice)
        self.assertEqual([list(extent) for extent in family.top], [])
        self.assertEqual(len(family.top), 0)
        self.assertNotIn(family.maximum, family.top)
        connection = GaloisConnection(context)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [list(extent) for extent in family.top],
            [["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"]],
        )
        extent = Extent(connection, elements=lattice.maximum.intent)
        self.assertIn(extent, family.top)

    def test_bottom(self):
        lattice = ExtensibleConceptLattice()
        family = ExtentLattice(lattice)
        self.assertEqual([list(extent) for extent in family.bottom], [])
        self.assertEqual(len(family.bottom), 0)
        self.assertNotIn(family.minimum, family.bottom)
        connection = GaloisConnection(context)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [list(extent) for extent in family.bottom],
            [[]],
        )
        extent = Extent(connection, elements=lattice.minimum.intent)
        self.assertIn(extent, family.bottom)

    def test_join_irreducible(self):
        lattice = ExtensibleConceptLattice()
        family = ExtentLattice(lattice)
        connection = GaloisConnection(context)
        extent = Extent(
            connection,
            elements=Intent(connection, elements=["Cat"]),
        )
        self.assertNotIn(extent, family.join_irreducible)
        self.assertEqual(len(family.join_irreducible), 0)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [list(extent) for extent in family.join_irreducible],
            [["Cat"], ["Owl", "Hawk", "Cat"], ["Fox", "Wolf"], ["Tiger", "Lion"]],
        )
        self.assertIn(extent, family.join_irreducible)
        self.assertEqual(len(family.join_irreducible), 4)

    def test_meet_irreducible(self):
        lattice = ExtensibleConceptLattice()
        family = ExtentLattice(lattice)
        connection = GaloisConnection(context)
        extent = Extent(
            connection,
            elements=Intent(connection, elements=["Fox", "Wolf"]),
        )
        self.assertNotIn(extent, family.meet_irreducible)
        self.assertEqual(len(family.meet_irreducible), 0)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [list(extent) for extent in family.meet_irreducible],
            [
                ["Fox", "Wolf"],
                ["Tiger", "Lion"],
                ["Owl", "Hawk", "Cat"],
                ["Fox", "Wolf", "Cat", "Tiger", "Lion"],
            ],
        )
        self.assertIn(extent, family.meet_irreducible)
        self.assertEqual(len(family.meet_irreducible), 4)

    def test_cover(self):
        lattice = ExtensibleConceptLattice()
        family = ExtentLattice(lattice)
        connection = GaloisConnection(context)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [(list(src), list(dest)) for src, dest in family.cover],
            [
                (
                    ["Fox", "Wolf", "Cat", "Tiger", "Lion"],
                    ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ),
                (
                    ["Owl", "Hawk", "Cat"],
                    ["Owl", "Hawk", "Eagle", "Fox", "Wolf", "Cat", "Tiger", "Lion"],
                ),
                (["Fox", "Wolf"], ["Fox", "Wolf", "Cat", "Tiger", "Lion"]),
                (["Tiger", "Lion"], ["Fox", "Wolf", "Cat", "Tiger", "Lion"]),
                (["Cat"], ["Fox", "Wolf", "Cat", "Tiger", "Lion"]),
                (["Cat"], ["Owl", "Hawk", "Cat"]),
                ([], ["Fox", "Wolf"]),
                ([], ["Tiger", "Lion"]),
                ([], ["Cat"]),
            ],
        )

    def test_order(self):
        lattice = ExtensibleConceptLattice()
        family = ExtentLattice(lattice)
        connection = GaloisConnection(context)
        lattice.extend(
            [
                Concept(connection, objects=["Dove"]),
                Concept(connection, objects=["Hen"]),
            ],
        )
        self.assertEqual(
            [(list(src), list(dest)) for src, dest in family.order],
            [
                (
                    ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                    ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ),
                (
                    ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                    ["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"],
                ),
                (
                    ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                    ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                ),
            ],
        )

    def test_object_extents(self):
        lattice = ExtensibleConceptLattice()
        family = ExtentLattice(lattice)
        connection = GaloisConnection(context)
        lattice.extend(
            [
                Concept(connection, objects=["Dove"]),
                Concept(connection, objects=["Hen"]),
            ],
        )
        self.assertEqual(
            [
                (list(extent), list(values))
                for extent, values in family.object_extents.items()
            ],
            [
                (
                    ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                    ["Dove", "Duck", "Goose", "Owl", "Hawk"],
                ),
                (["Dove", "Hen", "Duck", "Goose", "Owl", "Hawk"], ["Hen"]),
            ],
        )
        self.assertEqual(len(family.object_extents), 2)
        with self.assertRaises(KeyError):
            _ = family.object_extents[2]


class IntentFamilyTestCase(TestCase):
    def test___init__(self):
        connection = GaloisConnection(context)
        lattice = ExtensibleConceptLattice()
        lattice.extend(
            [
                Concept(connection, objects=["Dove"]),
                Concept(connection, objects=["Hen"]),
            ],
        )
        family = IntentLattice(lattice)
        self.assertEqual(
            [[str(attribute) for attribute in intent] for intent in family],
            [["small", "twolegs", "feathers", "fly"], ["small", "twolegs", "feathers"]],
        )
        self.assertEqual(
            [
                (
                    [str(attribute) for attribute in intent],
                    [str(attribute) for attribute in attributes],
                )
                for intent, attributes in family.attribute_intents.items()
            ],
            [
                (["small", "twolegs", "feathers"], ["small", "twolegs", "feathers"]),
                (["small", "twolegs", "feathers", "fly"], ["fly"]),
            ],
        )
        self.assertEqual(len(family), 2)

    def test_top(self):
        lattice = ExtensibleConceptLattice()
        family = IntentLattice(lattice)
        self.assertEqual([list(intent) for intent in family.top], [])
        self.assertEqual(len(family.top), 0)
        self.assertNotIn(family.maximum, family.top)
        connection = GaloisConnection(context)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [[str(attribute) for attribute in intent] for intent in family.top],
            [
                [
                    "small",
                    "twolegs",
                    "feathers",
                    "fly",
                    "swim",
                    "hunt",
                    "medium",
                    "fourlegs",
                    "hair",
                    "run",
                    "mane",
                    "big",
                    "hooves",
                ],
            ],
        )
        intent = Intent(connection, elements=lattice.minimum.extent)
        self.assertIn(intent, family.top)

    def test_bottom(self):
        lattice = ExtensibleConceptLattice()
        family = IntentLattice(lattice)
        self.assertEqual([list(intent) for intent in family.bottom], [])
        self.assertEqual(len(family.bottom), 0)
        self.assertNotIn(family.minimum, family.bottom)
        connection = GaloisConnection(context)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [list(intent) for intent in family.bottom],
            [[context.attribute("hunt")]],
        )
        intent = Intent(connection, elements=lattice.maximum.extent)
        self.assertIn(intent, family.bottom)

    def test_join_irreducible(self):
        lattice = ExtensibleConceptLattice()
        family = IntentLattice(lattice)
        connection = GaloisConnection(context)
        intent = Intent(
            connection,
            elements=Extent(
                connection,
                elements=[
                    context.attribute("small"),
                    context.attribute("hunt"),
                ],
            ),
        )
        self.assertNotIn(intent, family.join_irreducible)
        self.assertEqual(len(family.join_irreducible), 0)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [list(extent) for extent in family.join_irreducible],
            [
                [
                    context.attribute("hunt"),
                    context.attribute("medium"),
                    context.attribute("fourlegs"),
                    context.attribute("hair"),
                    context.attribute("run"),
                ],
                [
                    context.attribute("hunt"),
                    context.attribute("fourlegs"),
                    context.attribute("hair"),
                    context.attribute("run"),
                    context.attribute("big"),
                ],
                [
                    context.attribute("small"),
                    context.attribute("hunt"),
                ],
                [
                    context.attribute("hunt"),
                    context.attribute("fourlegs"),
                    context.attribute("hair"),
                    context.attribute("run"),
                ],
            ],
        )
        self.assertIn(intent, family.join_irreducible)
        self.assertEqual(len(family.join_irreducible), 4)

    def test_meet_irreducible(self):
        lattice = ExtensibleConceptLattice()
        family = IntentLattice(lattice)
        connection = GaloisConnection(context)
        intent = Intent(
            connection,
            elements=Extent(
                connection,
                elements=[
                    context.attribute("small"),
                    context.attribute("hunt"),
                ],
            ),
        )
        self.assertNotIn(intent, family.meet_irreducible)
        self.assertEqual(len(family.meet_irreducible), 0)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [list(intent) for intent in family.meet_irreducible],
            [
                [
                    context.attribute("small"),
                    context.attribute("hunt"),
                    context.attribute("fourlegs"),
                    context.attribute("hair"),
                    context.attribute("run"),
                ],
                [
                    context.attribute("small"),
                    context.attribute("hunt"),
                ],
                [
                    context.attribute("hunt"),
                    context.attribute("medium"),
                    context.attribute("fourlegs"),
                    context.attribute("hair"),
                    context.attribute("run"),
                ],
                [
                    context.attribute("hunt"),
                    context.attribute("fourlegs"),
                    context.attribute("hair"),
                    context.attribute("run"),
                    context.attribute("big"),
                ],
            ],
        )
        self.assertIn(intent, family.meet_irreducible)
        self.assertEqual(len(family.meet_irreducible), 4)

    def test_cover(self):
        lattice = ExtensibleConceptLattice()
        family = IntentLattice(lattice)
        connection = GaloisConnection(context)
        lattice.extend([Concept(connection, objects=["Owl", "Cat"])])
        lattice.extend([Concept(connection, objects=["Fox"])])
        lattice.extend([Concept(connection, objects=["Tiger"])])
        self.assertEqual(
            [
                (
                    [str(attribute) for attribute in src],
                    [str(attribute) for attribute in dest],
                )
                for src, dest in family.cover
            ],
            [
                (
                    ["small", "hunt", "fourlegs", "hair", "run"],
                    [
                        "small",
                        "twolegs",
                        "feathers",
                        "fly",
                        "swim",
                        "hunt",
                        "medium",
                        "fourlegs",
                        "hair",
                        "run",
                        "mane",
                        "big",
                        "hooves",
                    ],
                ),
                (
                    ["hunt", "medium", "fourlegs", "hair", "run"],
                    [
                        "small",
                        "twolegs",
                        "feathers",
                        "fly",
                        "swim",
                        "hunt",
                        "medium",
                        "fourlegs",
                        "hair",
                        "run",
                        "mane",
                        "big",
                        "hooves",
                    ],
                ),
                (
                    ["hunt", "fourlegs", "hair", "run", "big"],
                    [
                        "small",
                        "twolegs",
                        "feathers",
                        "fly",
                        "swim",
                        "hunt",
                        "medium",
                        "fourlegs",
                        "hair",
                        "run",
                        "mane",
                        "big",
                        "hooves",
                    ],
                ),
                (
                    ["hunt", "fourlegs", "hair", "run"],
                    ["small", "hunt", "fourlegs", "hair", "run"],
                ),
                (
                    ["hunt", "fourlegs", "hair", "run"],
                    ["hunt", "medium", "fourlegs", "hair", "run"],
                ),
                (
                    ["hunt", "fourlegs", "hair", "run"],
                    ["hunt", "fourlegs", "hair", "run", "big"],
                ),
                (["small", "hunt"], ["small", "hunt", "fourlegs", "hair", "run"]),
                (["hunt"], ["hunt", "fourlegs", "hair", "run"]),
                (["hunt"], ["small", "hunt"]),
            ],
        )

    def test_order(self):
        lattice = ExtensibleConceptLattice()
        family = IntentLattice(lattice)
        connection = GaloisConnection(context)
        lattice.extend(
            [
                Concept(connection, objects=["Dove"]),
                Concept(connection, objects=["Hen"]),
            ],
        )
        self.assertEqual(
            [
                (
                    [str(attribute) for attribute in src],
                    [str(attribute) for attribute in dest],
                )
                for src, dest in family.order
            ],
            [
                (
                    ["small", "twolegs", "feathers", "fly"],
                    ["small", "twolegs", "feathers", "fly"],
                ),
                (
                    ["small", "twolegs", "feathers"],
                    ["small", "twolegs", "feathers", "fly"],
                ),
                (["small", "twolegs", "feathers"], ["small", "twolegs", "feathers"]),
            ],
        )

    def test_attribute_intents(self):
        lattice = ExtensibleConceptLattice()
        family = IntentLattice(lattice)
        connection = GaloisConnection(context)
        lattice.extend(
            [
                Concept(connection, objects=["Dove"]),
                Concept(connection, objects=["Hen"]),
            ],
        )
        self.assertEqual(
            [
                (
                    [str(attribute) for attribute in intent],
                    [str(attribute) for attribute in values],
                )
                for intent, values in family.attribute_intents.items()
            ],
            [
                (["small", "twolegs", "feathers"], ["small", "twolegs", "feathers"]),
                (["small", "twolegs", "feathers", "fly"], ["fly"]),
            ],
        )
        self.assertEqual(len(family.attribute_intents), 2)
        with self.assertRaises(KeyError):
            _ = family.attribute_intents[2]
